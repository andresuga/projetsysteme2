//  un thread et le main exécutent la même fonction (faire) 
//  qui elle-même appelle une fonction (add)

#include "syscall.h"
int add(int nb){
	int k;
	for(k=0; k<1000; k++){
		nb ++;
	}
	return nb;
}



void faire(void * arg)
{
	 
	 
	int nombre = *((int *)arg);
	 
	int semaphore = *((int *)arg+1);
	 
	// affichage début de fonction
	SemP(semaphore);  
	PutString("(reçu) : ");
	 
	PutInt(nombre);
	PutChar('\n');
	SemV(semaphore);
	
	int resultat = add(nombre);
	
	// affichage fin de fonction
	SemP(semaphore);  
	PutString("(fin) : ");
	 
	PutInt(resultat);
	PutChar('\n');
	SemV(semaphore);
}
 


int main ()
{
	int semAffichage = SemaphoreInit("s",1); // pour affichage

	// data pour le thread
	 
	int data1[2];
	data1[0] = 19; 
	data1[1] = semAffichage;
	 
	int thread1 = UserThreadCreate(faire, data1);

	// data pour le main
	  
	int data2[2];
	data2[0] = 42; 
	data2[1] = semAffichage;
	
	faire(data2);
	
	UserThreadJoin(thread1);
	
   return 0;
}

#include "syscall.h"
// test de l'appel systeme Malloc


int main()
{
	PutString("debut\n");
	 
	int s = HeapSize();
	PutInt(s);
	PutChar('\n');
	
	int adresse1 = Malloc(80);
	PutString("Adresse 1 : ");
	PutInt(adresse1);
	PutChar('\n'); 
	
	
	
	int adresse2 = Malloc(40);
	PutString("Adresse 2 : ");
	PutInt(adresse2);
	PutChar('\n'); 
	
	
	int adresse3 = Malloc(60);
	PutString("Adresse 3 : ");
	PutInt(adresse3);
	PutChar('\n'); 
	
	int adresse4 = Malloc(1);
	PutString("Adresse 4 : ");
	PutInt(adresse4);
	PutChar('\n'); 
	
	Free(adresse3);
	Free(adresse1);
	Free(adresse2);
	
	
	
	 
	
	int adresse5 = Malloc(160);
	PutString("Adresse 5 : ");
	PutInt(adresse5);
	PutChar('\n');
	
	
	int adresse6 = Malloc(1);
	PutString("Adresse 6 : ");
	PutInt(adresse6);
	PutChar('\n');
	
	Free(adresse4);
	Free(adresse5);
	Free(adresse6);
	
	int adresse7 = Malloc(636); // trop grand
	PutString("Adresse 7 : ");
	PutInt(adresse7);
	PutChar('\n'); 
	
	int adresse8 = Malloc(632);
	PutString("Adresse 8 : ");
	PutInt(adresse8);
	PutChar('\n'); 
	
	adresse7 = Malloc(10); // trop grand
	PutString("Adresse 7 : ");
	PutInt(adresse7);
	PutChar('\n');
	//****************************************
	 
	HeapPlus();
	s = HeapSize();
	PutString("********** ");
	PutInt(s);
	PutChar('\n');
	// *********************************************
	adresse7 = Malloc(10); //  
	PutString("Adresse  : ");
	PutInt(adresse7);
	PutChar('\n');
	
	/*
	Free(adresse8);
	
	
	adresse5 = Malloc(300);
	PutString("Adresse 5 : ");
	PutInt(adresse5);
	PutChar('\n');
	adresse6 = Malloc(328);
	PutString("Adresse 6 : ");
	PutInt(adresse6);
	PutChar('\n');
	Free(adresse5);
	Free(adresse6);
	adresse5 = Malloc(300);
	PutString("Adresse 5 : ");
	PutInt(adresse5);
	PutChar('\n');
	PutString("fin\n");
	*/
	return 0;
}
  

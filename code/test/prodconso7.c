// producteur consommateur v2
// 2  producteurs et 1 consommateur

#include "syscall.h"
 

void produire(void *arg){
	// lecture des données	 
	int Plein = *((int *)arg);
	int Vide = *((int *)arg+1);
	int Mutex = *((int *)arg+2);
	int Mutex2 = *((int *)arg+8);
	int numIteration = *((int *)arg+3);
	int sizeTableau = *((int *)arg+4);
	int tableau = *((int *)arg+5);
	int *pointeur = (int *)tableau;
	int compteurProduction = *((int *)arg+6);
	int *ptrCompteur = (int *)compteurProduction;
	 
	int idThread = GetThreadId();
	int compteur;
	 
		
	int nombre = 29 + 100*idThread;
	int curseur;
	int suite = 1;
	// en action
	 
	while(suite){ 
		SemP(Mutex2);
			compteur = *ptrCompteur;
			if (compteur>= numIteration){suite = 0;}
			compteur ++;
			*ptrCompteur = compteur;
		SemV(Mutex2);
		
		if (suite){
			SemP(Vide);
			SemP(Mutex);
				
				PutString("\nproducteur (");
				PutInt(idThread);
				PutString(") : ");
				nombre++;
				PutInt(nombre);
				curseur = (compteur-1)%sizeTableau;
				*(pointeur + curseur) = nombre;
			SemV(Mutex);
			SemV(Plein);
		}
	}
	
	//PutString("\nFin production"); 
}

void consommer(void *arg){
	// lecture des données	 
	int Plein = *((int *)arg);
	int Vide = *((int *)arg+1);
	int Mutex = *((int *)arg+2);
	int numIteration = *((int *)arg+3);
	int sizeTableau = *((int *)arg+4);
	int tableau = *((int *)arg+5);
	int *pointeur = (int *)tableau;
	//int compteurConsommation = *((int *)arg+7);
	
	 
	
	
	int nombre;
	int curseur = 0;
	// en action
	int k;
	for (k=0; k<numIteration; k++){ 
		SemP(Plein);
		SemP(Mutex);
			nombre = *(pointeur + curseur);
			 
			curseur = (curseur+1)%sizeTableau;
			PutString("\n\nconsommateur : ");
			PutInt(nombre);
		SemV(Mutex);
		SemV(Vide);
	}
	 
}

// *********************************************************************
// *********************************************************************
// *********************************************************************
int main ()
{
	int a = 0; // pour initialiser pointeur
	int b=0;
	 
	PutString("Nombre d'itérations : ");
	int *iteration = &a;
	GetInt(iteration);
	//PutInt(*iteration);
	
	PutString("Taille du tableau : ");
	int *sizeTableau = &b;
	GetInt(sizeTableau);
	//PutInt(*sizeTableau); 
	   
	int Plein = SemaphoreInit("plein",0);  
	int Vide = SemaphoreInit("videx",*sizeTableau);  
	int Mutex = SemaphoreInit("mutex",1);  
	int Mutex2 = SemaphoreInit("mutex2",1);  
	int tableau = Malloc((*sizeTableau)*sizeof(int));
	int compteurProduction = Malloc(1*sizeof(int));
	int compteurConsommation = Malloc(1*sizeof(int));
	*((int *)compteurProduction) = 0;
	*((int *)compteurConsommation) = 0;
	
	 
	int data = Malloc(9*sizeof(int));
	*((int *)data)  = Plein; 
	*((int *)data + 1)  = Vide;
	*((int *)data + 2)  = Mutex;
	*((int *)data + 8)  = Mutex2;
	*((int *)data + 3)  = *iteration;
	*((int *)data + 4)  = *sizeTableau;
	*((int *)data + 5)  = tableau;
	*((int *)data + 6)  = compteurProduction;
	*((int *)data + 7)  = compteurConsommation;
	 
	 
	
	// creation des threads
	int producteur = UserThreadCreate(produire, (int *)data);
	int producteur2 = UserThreadCreate(produire, (int *)data);
	 
	int consommateur = UserThreadCreate(consommer, (int *)data);
	
	
	// join
	UserThreadJoin(producteur);
	UserThreadJoin(producteur2);
	 
	UserThreadJoin(consommateur);
	
	Free(tableau);
	Free(compteurProduction);
	Free(compteurConsommation);
	PutString("\nfin\n");
	
	return 0;
}
 

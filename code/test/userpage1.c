// test creation de thread

#include "syscall.h"

#define THIS "ccc"
#define THAT "ddd"
const int N = 10;

void puts(char *s)
{
	char *p;
	for(p=s; *p != '\0'; p++)
	    PutChar(*p);
}


void f(void *s)
{
	int i;
	for (i=0; i<N; i++)
		puts((char *)s);
	
}
	 

int main ()
{
		int t = UserThreadCreate(f, (void *)THIS);
		f((void*) THAT);
		UserThreadJoin(t);
   return 0;
}

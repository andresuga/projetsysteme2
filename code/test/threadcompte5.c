// appel "récursif" de threads

#include "syscall.h"
 

void faire(void * arg)
{
	int k, nombre;
	
	int adresse = *((int *)arg);
	int *pointeur = (int *)adresse;
	int semA = *((int *)arg+1);
	int semC = *((int *)arg+2);
	int encore = *((int *)arg+3);
	
	int idThread = GetThreadId();
	
	SemP(semA);
	PutString("\nNouveau thread, id = ");
		PutInt(idThread);
		
		PutString("\nencore : ");
		PutInt(encore);
		PutChar('\n');
		PutInt(idThread);   
		PutString(" : debut = ");
		nombre = *pointeur;
		PutInt(nombre);
		PutChar('\n');
	SemV(semA);
	
	int threadF=-2;
	if (encore>0){
		*((int *)arg+3) = encore-1;
		threadF = UserThreadCreate(faire, arg);
	}
	
	// boucle
	for(k=0; k<1000; k++){
		SemP(semC);
		nombre = *pointeur;
		nombre ++;
		*pointeur = nombre;
		SemV(semC);
	}
	
	if (encore>0 && threadF>0)
			UserThreadJoin(threadF);
	 
		
	SemP(semA);  
		PutString("\nThread ");
		PutInt(idThread);   
		PutString(" : fin = ");
		nombre = *pointeur;
		PutInt(nombre);
		PutChar('\n');
	SemV(semA);

}
 
// *********************************************************************

int main ()
{
	int z = 0; // valeur initiale arbitraire
	int *ptr = &z;
	    
	int semAffichage = SemaphoreInit("s",1); // pour affichage
	int semCalcul = SemaphoreInit("t",1); // pour section critique
	 
	int info[4];
	info[0] = (int)ptr; 
	info[1] = semAffichage;
	info[2] = semCalcul;
	info[3] = 3; // nombre d'appels restants
	
	int k;
	int N = 2;
 
	int threads[N];
	for(k=0; k<N; k++){
		threads[k] = UserThreadCreate(faire, info);
	}
	
	 
	for(k=0; k<N; k++){
		if (threads[k]>0)
			UserThreadJoin(threads[k]);
	}
	 
	SemP(semAffichage);  
	PutString("\nValeur finale : ");
	int nombre = *ptr;
	PutInt(nombre);
	PutChar('\n');
	SemV(semAffichage);
	 
	
   return 0;
}

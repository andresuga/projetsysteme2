#include "syscall.h"
// test de l'appel systeme Malloc


int main()
{
	PutString("debut\n");
	 
	int s = HeapSize();
	PutInt(s);
	PutChar('\n');
	
	int adresse;
	int k;
	
	
	for (k=1; k<s+2; k++){
		adresse=Malloc(k);
		PutString("Allocation de ");
		PutInt(k);
		PutString(" octets.\n");
		
		PutString("  Adresse retour : ");
		PutInt(adresse);
		PutChar('\n'); 
		
		if (adresse>0)
			{Free(adresse);}
	}
	
	int brk = HeapPlus();
	PutString("\nHeapPlus : ");
	PutInt(brk);
	PutString(" \n\n");
	
	if (brk>0){
		for (k=s-5; k<s+130; k++){
			adresse=Malloc(k);
			PutString("Allocation de ");
			PutInt(k);
			PutString(" octets.\n");
			
			PutString("  Adresse retour : ");
			PutInt(adresse);
			PutChar('\n'); 
			
			if (adresse>0)
				{Free(adresse);}
		}
	}
	return 0;
}
  

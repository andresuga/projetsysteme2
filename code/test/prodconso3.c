// producteur consommateur v2
// 2  producteurs et 1 consommateur

#include "syscall.h"
 

void produire(void *arg){
	// lecture des données	 
	int Plein = *((int *)arg);
	int Vide = *((int *)arg+1);
	int Mutex = *((int *)arg+2);
	int Mutex2 = *((int *)arg+8);
	int numIteration = *((int *)arg+3);
	int sizeTableau = *((int *)arg+4);
	int tableau = *((int *)arg+5);
	int *pointeur = (int *)tableau;
	
	int compteurProduction = *((int *)arg+6);
	int *ptrCompteur = (int *)compteurProduction;
	 
	int idThread = GetThreadId();
	int compteurP;
	
	/*
	SemP(Mutex);
		PutString("\nproducteur  : "); 
		PutInt(idThread);
	SemV(Mutex);
	*/
	
	int nombre = 29 + 100*idThread;
	int curseur;
	int suite = 1;
	// en action
	 
	while(suite){ 
		SemP(Mutex2);
			compteurP = *ptrCompteur;
			if (compteurP>= numIteration){suite = 0;}
			compteurP ++;
			*ptrCompteur = compteurP;
		SemV(Mutex2);
		
		if (suite){
			SemP(Vide);
			SemP(Mutex);
				PutString("\nproducteur   ");
				PutInt(idThread);
				PutString(" (");
				PutInt(compteurP);
				PutString(") : ");
				nombre++;
				PutInt(nombre);
				curseur = (compteurP-1)%sizeTableau;
				*(pointeur + curseur) = nombre;
			SemV(Mutex);
			SemV(Plein);
			}
		
	}
}
// *********************************************************************
// *********************************************************************
void consommer(void *arg){
	// lecture des données	 
	int Plein = *((int *)arg);
	int Vide = *((int *)arg+1);
	int Mutex = *((int *)arg+2);
	int Mutex3 = *((int *)arg+9);
	int numIteration = *((int *)arg+3);
	int sizeTableau = *((int *)arg+4);
	int tableau = *((int *)arg+5);
	int *pointeur = (int *)tableau;
	
	int compteurConsommation = *((int *)arg+7);
	int *ptrCompteur = (int *)compteurConsommation;
	 
	int idThread = GetThreadId();
	int compteurC;
	
	/*
	SemP(Mutex);
		PutString("\nconsommateur  : "); 
		PutInt(idThread);
	SemV(Mutex);
	*/
	
	int nombre;
	int curseur = 0;
	
	int suite = 1;
	// en action
	while(suite){ 
		SemP(Mutex3);
			compteurC = *ptrCompteur;
			if (compteurC >= numIteration){suite = 0;}
			compteurC ++;
			*ptrCompteur = compteurC;
		SemV(Mutex3);
		
		if (suite){

			SemP(Plein);
			SemP(Mutex);
				curseur = (compteurC-1)%sizeTableau;
				nombre = *(pointeur + curseur);

				PutString("\nconsommateur ");
				PutInt(idThread);
				PutString(" (");
				PutInt(compteurC);
				PutString(") : ");
				PutInt(nombre);
			SemV(Mutex);
			SemV(Vide);
		}
	}
	
	 
}

// *********************************************************************
// *********************************************************************
// *********************************************************************
int main ()
{
	int a = 0; // pour initialiser pointeur
	int b=0;
	 
	PutString("Nombre d'itérations : ");
	int *iteration = &a;
	GetInt(iteration);
	//PutInt(*iteration);
	
	PutString("Taille du tableau : ");
	int *sizeTableau = &b;
	GetInt(sizeTableau);
	//PutInt(*sizeTableau); 
	   
	int Plein = SemaphoreInit("plein",0);  
	int Vide = SemaphoreInit("videx",*sizeTableau);  
	int Mutex = SemaphoreInit("mutex",1);  
	int Mutex2 = SemaphoreInit("mutex2",1);  
	int Mutex3 = SemaphoreInit("mutex3",1);  
	int tableau = Malloc((*sizeTableau)*sizeof(int));
	int compteurProduction = Malloc(1*sizeof(int));
	int compteurConsommation = Malloc(1*sizeof(int));
	*((int *)compteurProduction) = 0;
	*((int *)compteurConsommation) = 0;
	
	 
	int data = Malloc(10*sizeof(int));
	*((int *)data)  = Plein; 
	*((int *)data + 1)  = Vide;
	*((int *)data + 2)  = Mutex;
	*((int *)data + 8)  = Mutex2;
	*((int *)data + 9)  = Mutex3;
	*((int *)data + 3)  = *iteration;
	*((int *)data + 4)  = *sizeTableau;
	*((int *)data + 5)  = tableau;
	*((int *)data + 6)  = compteurProduction;
	*((int *)data + 7)  = compteurConsommation;
	 
	 
	
	// creation des threads
	int producteur = UserThreadCreate(produire, (int *)data);
	int producteur2 = UserThreadCreate(produire, (int *)data);
	int producteur3 = UserThreadCreate(produire, (int *)data);
	 
	int consommateur = UserThreadCreate(consommer, (int *)data);
	int consommateur2 = UserThreadCreate(consommer, (int *)data);
	
	
	// join
	UserThreadJoin(producteur);
	UserThreadJoin(producteur2);
	UserThreadJoin(producteur3);
	 
	UserThreadJoin(consommateur);
	UserThreadJoin(consommateur2);
	
	Free(tableau);
	Free(compteurProduction);
	Free(compteurConsommation);
	PutString("\nfin\n");
	
	return 0;
}
 

// reprend la code de threadcompte2 (synchro ok)
// cette fois N threads pour tester les limites !

#include "syscall.h"
 

void faire(void * arg)
{
	int k, nombre;
	
	int adresse = *((int *)arg);
	int *pointeur = (int *)adresse;
	//int semA = *((int *)arg+1);
	int semC = *((int *)arg+2);
	 
	// affichage
	/*
	SemP(semA);  
	PutString("(faire1) : ");
	nombre = *pointeur;
	PutInt(nombre);
	PutChar('\n');
	SemV(semA);
	*/
	
	// boucle
	for(k=0; k<1000; k++){
		SemP(semC);
		nombre = *pointeur;
		nombre ++;
		*pointeur = nombre;
		SemV(semC);
	}
	
	/*
	// affichage
	SemP(semA);  
	PutString("(faire2) : ");
	nombre = *pointeur;
	PutInt(nombre);
	PutChar('\n');
	SemV(semA);
	*/
	
	//UserThreadExit();
}
 


int main ()
{
	int z = 0; // valeur initiale arbitraire
	int *ptr = &z;
	    
	int semAffichage = SemaphoreInit("s",1); // pour affichage
	int semCalcul = SemaphoreInit("t",1); // pour affichage
	 
	int data[3];
	data[0] = (int)ptr; // adresse de la donnée partagée
	data[1] = semAffichage;
	data[2] = semCalcul;
	
	
	int k, nombre;
	int N;
	
	/*
	PutString("combien ? ");
	N = (int)(GetChar()-48);
	*/
	N = 12;
	
	
	int threads[2*N];
	
	// tentative de creation de N threads
	PutString("\nCreation de 12 threads\n");
	for(k=0; k<N; k++){
		threads[k] = UserThreadCreate(faire, data);
		SemP(semAffichage);  
		PutString("Iteration ");
		PutInt(k);
		PutString(" : ");
		PutInt(threads[k]);
		PutChar('\n');
		SemV(semAffichage);
	}
	
	// join
	for(k=0; k<N; k++){
		if (threads[k]>0)
			UserThreadJoin(threads[k]);
	}
	
	PutString("\nAprès l'appel à Join\n");
	PutString("Creation de 12 threads\n");
	// affichage
	/*
	SemP(semAffichage);  
	PutString("(main) : ");
	nombre = *ptr;
	PutInt(nombre);
	PutChar('\n');
	SemV(semAffichage);
	* */
	
	// tentative de creation de N threads
	for(k=N; k<2*N; k++){
		threads[k] = UserThreadCreate(faire, data);
		SemP(semAffichage);  
		PutString("Iteration ");
		PutInt(k);
		PutString(" : ");
		PutInt(threads[k]);
		PutChar('\n');
		SemV(semAffichage);
	}
	
	
	// join
	for(k=N; k<2*N; k++){
		if (threads[k]>0)
			UserThreadJoin(threads[k]);
	}
	
	// affichage
	SemP(semAffichage);  
	PutString("\nValeur finale : ");
	nombre = *ptr;
	PutInt(nombre);
	PutChar('\n');
	SemV(semAffichage);
	 
	
   return 0;
}

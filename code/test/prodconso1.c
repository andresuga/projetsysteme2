// producteur consommateur
// 1 producteur et 1 consommateur

#include "syscall.h"
 

void produire(void *arg){
	// lecture des données	 
	int Plein = *((int *)arg);
	int Vide = *((int *)arg+1);
	int Mutex = *((int *)arg+2);
	int numIteration = *((int *)arg+3);
	int sizeTableau = *((int *)arg+4);
	int tableau = *((int *)arg+5);
	int *pointeur = (int *)tableau;
	
	int nombre = 29;
	int curseur = 0;
	// en action
	int k = 0;
	for (k=0; k<numIteration; k++){ 
		SemP(Vide);
		SemP(Mutex);
			PutString("\nproducteur : ");
			PutInt(nombre);
			*(pointeur + curseur) = nombre;
			nombre++;
			curseur = (curseur+1)%sizeTableau;
		SemV(Mutex);
		SemV(Plein);
	}
	 
}

void consommer(void *arg){
	// lecture des données	 
	int Plein = *((int *)arg);
	int Vide = *((int *)arg+1);
	int Mutex = *((int *)arg+2);
	int numIteration = *((int *)arg+3);
	int sizeTableau = *((int *)arg+4);
	int tableau = *((int *)arg+5);
	int *pointeur = (int *)tableau;
	
	int nombre;
	int curseur = 0;
	// en action
	int k = 0;
	for (k=0; k<numIteration; k++){ 
		SemP(Plein);
		SemP(Mutex);
			nombre = *(pointeur + curseur);
			 
			curseur = (curseur+1)%sizeTableau;
			PutString("\nconsommateur : ");
			PutInt(nombre);
		SemV(Mutex);
		SemV(Vide);
	}
	 
}

// *********************************************************************
// *********************************************************************
// *********************************************************************
int main ()
{
	int a = 0; // pour initialiser pointeur
	int b=0;
	 
	PutString("Nombre d'itérations : ");
	int *iteration = &a;
	GetInt(iteration);
	//PutInt(*iteration);
	
	PutString("Taille du tableau : ");
	int *sizeTableau = &b;
	GetInt(sizeTableau);
	//PutInt(*sizeTableau); 
	   
	int Plein = SemaphoreInit("plein",0);  
	int Vide = SemaphoreInit("videx",*sizeTableau);  
	int Mutex = SemaphoreInit("mutex",1);  
	 
	int tableau = Malloc((*sizeTableau)*sizeof(int));
	
	int data = Malloc(6*sizeof(int));
	*((int *)data)  = Plein; 
	*((int *)data + 1)  = Vide;
	*((int *)data + 2)  = Mutex;
	*((int *)data + 3)  = *iteration;
	*((int *)data + 4)  = *sizeTableau;
	*((int *)data + 5)  = tableau;
	 
	
	// creation des threads
	int producteur = UserThreadCreate(produire, (int *)data);
	 
	int consommateur = UserThreadCreate(consommer, (int *)data);
	
	
	// join
	UserThreadJoin(producteur);
	 
	UserThreadJoin(consommateur);
	
	PutString("\nfin\n");
	
	return 0;
}
 

// reprend la code de threadcompte2 (synchro ok)
// cette fois N threads pour tester les limites !

#include "syscall.h"
 

void faire(void * arg)
{
	int k, nombre;
	
	int adresse = *((int *)arg);
	int *pointeur = (int *)adresse;
	//int semA = *((int *)arg+1);
	int semC = *((int *)arg+2);

	
	// boucle
	for(k=0; k<1000; k++){
		SemP(semC);
		nombre = *pointeur;
		nombre ++;
		*pointeur = nombre;
		SemV(semC);
	}

}
 


int main ()
{
	PutString("Valeur pour temporisation ? ");
	int tempo;
	GetInt(&tempo);
	
	
	int z = 0; // valeur initiale arbitraire
	int *ptr = &z;
	    
	int semAffichage = SemaphoreInit("s",1); // pour affichage
	int semCalcul = SemaphoreInit("t",1); // pour section critique
	 
	int data[3];
	data[0] = (int)ptr; // adresse de la donnée partagée
	data[1] = semAffichage;
	data[2] = semCalcul;
	
	
	int k, nombre;
	
	int N = 12;
	int threads[2*N];
	
	// tentative de creation de N threads
	PutString("\nCreation de 12 threads\n");
	for(k=0; k<N; k++){
		threads[k] = UserThreadCreate(faire, data);
		SemP(semAffichage);  
		PutString("Iteration ");
		PutInt(k);
		PutString(" : ");
		PutInt(threads[k]);
		PutChar('\n');
		SemV(semAffichage);
	}
	
	// temporisation
	int nbr = 0;
	for(k=0; k<tempo; k++){
		nbr++;
	}
	
	PutString("\nAprès la temporisation\n");
	PutString("Creation de 12 threads\n");
 
	
	// tentative de creation de N threads
	for(k=N; k<2*N; k++){
		threads[k] = UserThreadCreate(faire, data);
		SemP(semAffichage);  
		PutString("Iteration ");
		PutInt(k);
		PutString(" : ");
		PutInt(threads[k]);
		PutChar('\n');
		SemV(semAffichage);
	}
	
	
	// join
	for(k=0; k<2*N; k++){
		if (threads[k]>0)
			UserThreadJoin(threads[k]);
	}
	
	// affichage
	SemP(semAffichage);  
	PutString("\nValeur finale : ");
	nombre = *ptr;
	PutInt(nombre);
	PutChar('\n');
	SemV(semAffichage);
	 
	
   return 0;
}

// reprend la code de threadcompte1
// deux threads
// donc trois boucles en parallele

// le resultat attendu est 3019
// permet d'observer synchronisation grace à 2nd semaphore

#include "syscall.h"
 

void faire(void * arg)
{
	int k, nombre;
	
	int adresse = *((int *)arg);
	int *pointeur = (int *)adresse;
	int semA = *((int *)arg+1);
	int semC = *((int *)arg+2);
	 
	// affichage
	SemP(semA);  
	PutString("(debut) : ");
	nombre = *pointeur;
	PutInt(nombre);
	PutChar('\n');
	SemV(semA);
	
	// boucle avec section critique protegée
	for(k=0; k<1000; k++){
		SemP(semC);
		nombre = *pointeur;
		nombre ++;
		*pointeur = nombre;
		SemV(semC);
	}
	
	// affichage
	SemP(semA);  
	PutString("(fin) : ");
	nombre = *pointeur;
	PutInt(nombre);
	PutChar('\n');
	SemV(semA);
	
}
 


int main ()
{
	int z = 19; // valeur initiale arbitraire
	int *ptr = &z;
	    
	int semAffichage = SemaphoreInit("s",1); // pour affichage
	int semCalcul = SemaphoreInit("t",1); // pour affichage
	 
	int data[3];
	data[0] = (int)ptr; // adresse de la donnée partagée
	data[1] = semAffichage;
	data[2] = semCalcul;
	
	// creation des threads
	int thread1 = UserThreadCreate(faire, data);
	int thread2 = UserThreadCreate(faire, data);
	faire(data);
	 
	
	// join
	UserThreadJoin(thread1);
	UserThreadJoin(thread2);
	
	
	// affichage
	SemP(semAffichage);  
	PutString("(main) : ");
	int nombre = *ptr;
	PutInt(nombre);
	PutChar('\n');
	SemV(semAffichage);
	 
	
   return 0;
}

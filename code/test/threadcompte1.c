// test deux boucles en parallele
// le resultat attendu (!!!) est 2019
// permet d'observer un programme non synchronisé
// le semaphore permet d'avoir affichage propre


#include "syscall.h"
 

void faire(void * arg)
{
	int k, nombre;
	 
	int adresse = *((int *)arg);
	int *pointeur = (int *)adresse;
	//int semaphore = *((int *)arg+1);
	 
	// affichage début de fonction
	/*
	SemP(semaphore);  
	PutString("(faire1) : ");
	nombre = *pointeur;
	PutInt(nombre);
	PutChar('\n');
	SemV(semaphore);
	*/
	 
	// boucle
	for(k=0; k<1000; k++){
		nombre = *pointeur;
		nombre ++;
		*pointeur = nombre;
	}
	
	// affichage fin de fonction
	/*
	SemP(semaphore);  
	PutString("(faire2) : ");
	nombre = *pointeur;
	PutInt(nombre);
	PutChar('\n');
	SemV(semaphore);
	*/ 
	 
}
 


int main ()
{
	int z = 19; // valeur initiale arbitraire
	int *ptr = &z;
	    
	int semAffichage = SemaphoreInit("s",1); // pour affichage
	 
	int data[2];
	data[0] = (int)ptr; 
	data[1] = semAffichage;
	 
	int thread1 = UserThreadCreate(faire, data);
	faire(data);
	
	
	UserThreadJoin(thread1);
	
	// affichage
	int nombre;
	 
	SemP(semAffichage);  
	PutString("\n\nValeur finale : ");
	nombre = *ptr;
	PutInt(nombre);
	PutChar('\n');
	SemV(semAffichage);
	
   return 0;
}

// test de création de processus avec l'appel systeme ForkExec()

#include "syscall.h"
 
 


int main ()
{
	PutString("debut\n");
	 
	//ForkExec("consoletest1");
	//ForkExec("consoletest1");
	
	/* 
	ForkExec("threads_3a");
	ForkExec("threads_3a");
	*/
	//ForkExec("putstring");
	//ForkExec("threads_0");
	ForkExec("prodconso0");
	ForkExec("prodconso0");
	ForkExec("prodconso0");
	 
	
	/*
	ForkExec("putchar");
	ForkExec("putchar");
	ForkExec("threads_1");
	ForkExec("allocdynamique1"); // peut poser probleme
	*/
	
	PutString("fin\n");
	
	return 0;
}

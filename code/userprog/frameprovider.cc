// frameprovider 
//  gestion des cadres de page de la machine MIPS

#ifdef CHANGED

#include "copyright.h"
#include "system.h"
#include "frameprovider.h"
 


//----------------------------------------------------------------------
// FrameProvider::FrameProvider
//  
//----------------------------------------------------------------------

FrameProvider::FrameProvider(int size)
{
	frameTable = new int[size];
	
	frameTableSize = size;
	//victime = -1;
	int i;
	for (i=0; i<size; i++){
		frameTable[i]=-1;
	}
}



//----------------------------------------------------------------------
// FrameProvider::~FrameProvider
// 	Clean up ....
//----------------------------------------------------------------------

FrameProvider::~FrameProvider()
{
   delete [] frameTable;
}





//----------------------------------------------------------------------
// FrameProvider::Find 
//      Return the number of the first frame which is free.
//      
//
//      If no frame are clear, return -1.
//----------------------------------------------------------------------

int
FrameProvider::Find (int idSpace)
{
    
    int idFrame;
    // doit fournir un indice de cadre pour ecriture
    // on commence par chercher si un cadre est libre 
    for (idFrame = 0; idFrame < frameTableSize; idFrame++){
		if (Test (idFrame))
			{   
				//printf("find(1) : %d \n", idFrame);
				return idFrame;
			}
	  }
	// sinon, aucun cadre disponible
	//int actuel = currentThread->space->idAddrSpace;
	int actuel = idSpace;
	
	for (idFrame = 0; idFrame < frameTableSize; idFrame++)
		if (frameTable[idFrame] != actuel)
			{   
				// printf("find(2) : %d \n", idFrame);
				return idFrame;
			}
	  
	ASSERT(idFrame == frameTableSize); // ne doit pas être atteint !
    return -1;
}




//----------------------------------------------------------------------
// FrameProvider::FindIdFrame
// 	 renvoie un idFrame
//----------------------------------------------------------------------
int
FrameProvider::FindIdFrame(int victime)
{
	int idFrame=0;
	int idSpace = frameTable[idFrame];
	printf("recherche %d \n", idSpace);
	
	while(idFrame<frameTableSize && idSpace != victime){
		idFrame++;
		idSpace = frameTable[idFrame];
	}
	
	if (idFrame==frameTableSize)
		return -1;
	else 
		return idFrame;
	
}


//----------------------------------------------------------------------
// FrameProvider::Ask
//      .
//----------------------------------------------------------------------

int
FrameProvider::Ask(int idFrame)
{
    return frameTable[idFrame];
}

//----------------------------------------------------------------------
// FrameProvider::NumAvailFrame
//      .
//----------------------------------------------------------------------

int
FrameProvider::NumAvailFrame()
{
    int compteur = 0;
    int i;
    for (i = 0; i < frameTableSize; i++)
	if (frameTable[i] == -1)
	  {   
	       compteur ++;
	  }
    return compteur;
}


//----------------------------------------------------------------------
// FrameProvider::Test
//      Return TRUE if the "nth" frame is free.
//
//      "which" is the number of the frame to be tested.
//----------------------------------------------------------------------

bool
FrameProvider::Test (int which)
{
    ASSERT (which >= 0 && which < frameTableSize);

    if (frameTable[which] == -1)
	return TRUE;
    else
	return FALSE;
}



//----------------------------------------------------------------------
// FrameProvider::Print 
//   	...
//----------------------------------------------------------------------

void
FrameProvider::Print(){
	int i;
	printf(" \nTable des cadres de pages de la machine MIPS \n");
	for(i=0; i<NumPhysPages; i++){
		printf("cadre %i : processus %d\n", i, frameTable[i]);
	}
}


//----------------------------------------------------------------------
// FrameProvider::Record
//   	...
//----------------------------------------------------------------------

void
FrameProvider::Record(int idFrame, int idSpace){
	// indice identifie un cadre de pages
	frameTable[idFrame] = idSpace;
}

 

#endif


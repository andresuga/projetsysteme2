// heap.h 

#ifdef CHANGED 

#ifndef HEAP_H
#define HEAP_H

#include "copyright.h"
#include "utility.h"
  


// classe Heap ********************************************************
class Heap {
	public:
		Heap(int nbPages, int pointeurPage);
				// pointeurPage est la valeur renvoyée par la méthode AddrSpace::SbrkAddr(int nombrePages)
		~Heap();
		
		void HeapInit();
		
		int HeapAlloc(int nbBytes);
		void HeapFree(int adresse);
		 
		int GetMemoryAdr();  
		void SetMemoryAdr(int indicePage);  // ********
	
		int GetMemorySize(); 
		void SetMemorySize(int nbPages); // *********
		
		void AddPageMemory();
		
		
		
	private:
		bool WriteHeap(int heapAddress, int nbBytes, int value); // adresse relative du tas (débute à 0)
		bool ReadHeap(int heapAddress, int nbBytes, int *value); // adresse relative du tas (débute à 0)
		
		int HeapFitFirst(int nbBytes);
		
	
		int SearchPreviousFB(int address);
		void EcrireUB(int address,  int nbBytes);
		void EcrireDataFB(int address, int adressNextFB, int sizeFB);
		 
		int AddressFirstFB();
		int AddressLastFB();
		int LireSizeFB(int addressFB);
		int LireNextFB(int addressFB);
		 
		void EcrireNextFB(int addressFB, int addressNextFB);
		bool FusionDroite(int addressFB);
		
	
		int heapSize;
		int addressFirst; // adresse virtuelle du premier octet
		
		int sizeUB;
		
		 
		int minSizeBlock;
		// champs utilisés pour un malloc
		int addressePointeurAllocationFB; //adresse ou est écrite l'adresse du FB utilisé pour l'allocation; doit être mise à jour par HeapAlloc
		int adresseAllocationFB; // adresse du FB utilisé pour l'allocation;
};

#endif // HEAP_H

#endif // CHANGED

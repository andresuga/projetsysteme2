// userprocess.cc

#include "copyright.h"
#include "system.h"
#include "syscall.h"
#include "userprocess.h"




void StartUserProcess(int nb){
	// executé lorsque le nouveau thread sera appelé pour la première fois
	
	AddrSpace *space = currentThread->space;
	
	// chargement en mémoire physique
	space->RestoreState();
	
	// initialisation des registres
	space->InitRegisters();
	
	// par défaut 5 pages pour le tas
	space->SetUpHeap();
 
	 
    
    machine->Run();
    // jamais atteint
    ASSERT(FALSE);
}



// ****************************************************************************************
//                           do_UserProcessCreate
//
// ******************************************************************************************

int do_UserProcessCreate(char *buffer)
{  
		buffer[0]='b';
		buffer[1]='u';
		buffer[2]='i';
		buffer[3]='l';
		buffer[4]='d';
		buffer[5]='/';
		
		// currentThread est le thread qui utilise l'appel systeme ForkExec()
		
		// 1) appel au constructeur
		Thread *t = new Thread("processus nouveau");
		
		
		// 2) on n'appelle pas Fork() [ car reste dans le même space]
		// on reprend donc le travail effectué par Fork()
		
		// 2a) StackAllocate
		// il faut allouer la pile du thread (coté nachos) dans le tas du processus nachos
		// transmettre une réfrence de fonction qui sera exécutée au lancement
		int entier = 100; // arbitraire, valeur non utilisée
		t->StackAllocate(StartUserProcess, entier);
		
		
		// 2b) associer au thread son space
		// ce space n'existe pas donc il faut le créér
		// il faut construire un nouvel objet de la classe AddrSpace
		// avec pour argument : le nom de l'executable 
		OpenFile *executable2 = fileSystem->Open(buffer);
		
		if (executable2 == NULL)
			{
				printf (" \nUnable to open file %s\n", buffer);
				//break; ******** errreur ??
				return -1;
				//return; // *********************************************************  en boucle, gerer cas d'erreur !!
			}
			
		AddrSpace *space2;
		space2 = new AddrSpace(executable2);
		t->space = space2;
		delete executable2;
		
		int idProcessus = space2->idAddrSpace;
		 
		t->identifiant = 0;
		 
		
		// 2c) le nouveau thread doit être placé dans le scheduler
		IntStatus oldLevel = interrupt->SetLevel (IntOff); // ************************************** vraiment utile ???
		scheduler->ReadyToRun (t);	// ReadyToRun assumes that interrupts  are disabled!
		(void) interrupt->SetLevel (oldLevel);
        return idProcessus; 
}


// tablesemaphore.h 

#ifdef CHANGED 

#ifndef TABLESEMAPHORE_H
#define TABLESEMAPHORE_H

#include "copyright.h"
#include "utility.h"



class SemaphoreEntry{
	public :
		int valeurSem;
		int idSem;
	};


// ************************************************************************
// ************************************************************************

#define NumSemaphores 10


class TableSemaphore {
	
	public:
		TableSemaphore();
				// initialize the  
		~TableSemaphore();			// clean up console emulation
		
		// external interface 
		
		int Add(int valeurSem);
		int Full();
		int Ask(int idSem);
		void Remove(int idSem);
		
		
	private:
		int SearchRang(int idSem);
		int SearchFreePlace();
		
		int sizeTable;
		int effectif;
		SemaphoreEntry *tableDesSemaphores;
		int distributeurIdentifiant;
	};

#endif // TABLESEMAPHORE_H

#endif // CHANGED


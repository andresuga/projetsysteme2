// userthread.cc

#include "copyright.h"
#include "system.h"
#include "syscall.h"
#include "userthread.h"
#include "synch.h"



// ****************************************************************************************
//                           void StartUserThread(int nb)
// 
//     executé par le thread qui vient d'être créé. premier appel par l'ordonnanceur !
// 
//     printf("[StartUserThread] debut,   verification argument : valeur : %d , %x   *************]\n\n",nb, nb);
//     currentThread->Print();
// ******************************************************************************************

void StartUserThread(int nb)
{
     //printf("----- fonction StartUserThread ------------- \n");
    int *ptr =(int *) nb;
    int adresseMIPSFonction = *ptr;
    int adresseArgumentPourLaFonction = *(ptr+1); 
    
     
    
	// le thread à le statut RUNNING pour la première fois !!!
    currentThread->space->RestoreState();  // pour charger en mémoire MIPS (si besoin) l'espace mémoire du processus et la table des pages
    
	// on pourrait utiliser AddrSpace::InitRegisters() !! non car PCReg n'est pas initialisé à 0 !
	int i;
    for (i = 0; i < NumTotalRegs; i++)
		machine->WriteRegister (i, 0);

    machine->WriteRegister(PCReg, adresseMIPSFonction); 
    machine->WriteRegister(NextPCReg, adresseMIPSFonction + 4);
    
    machine->WriteRegister(4, adresseArgumentPourLaFonction);  // registre dédié r4 contient l'argument (un pointeur ... )
    
    // registre 31 pour tuer le thread en fin d'execution
    machine->WriteRegister(31, -1);  // registre dédié r4 contient l'argument (un pointeur ... )
    
	//pointeur de pile
	int pageStack = currentThread->space->AskPageStack(currentThread->rang); // rang
	
	int pointeurPileThread = (pageStack+1)*PageSize-16;
    machine->WriteRegister(StackReg, pointeurPileThread);
    
    // appels multiples (concurrents) à la méthode Run()
    machine->Run();
	
	// jamais atteint
	ASSERT(FALSE);
}



// ****************************************************************************************
// ****************************************************************************************
//                           do_UserThreadCreate
//
//     executée par le thread appelant
//     les arguments 'f' et 'arg' reçus sont accessibles directement par lecture des registre !!!
//     faut-il modifier la signature de la fonction ??
// ******************************************************************************************
// ******************************************************************************************

int do_UserThreadCreate(int f, int arg)
{  
	int retour;
	
	// pointeur de pile
	AddrSpace * space = currentThread->space;
	
	Semaphore *sem = currentThread->space->semaphoreGestionThreads;
	 
	sem->P(); // vraiment nécessaire ??
	int rangThread = space->AjouteThread(); // page du stack
	sem->V();
	
	if (rangThread < 0){
		//printf("***********************************************************   \nPile pleine !!");
		retour = -1;
	} else {
		// inscription des données dans le tas pour transmission via l'appel Fork() à la fonction StartUserThread
		//  inscription possible dans la pile du nouveau thread ? non car la pile du nouveau thread n'est pas encore allouée
		int *adresse;
		adresse = new int[2];  // dans le tas du processus nachos !!!
		*adresse = f; // (arg1)
		*(adresse+1) = arg;  // (arg 2)
		
		Thread *t = new Thread("joli nom");
		
		
		t->rang = rangThread;   
		t->identifiant = space->EnregistreThread(rangThread);
		retour = t->identifiant;

		t->Fork(StartUserThread,(int) adresse); 
	}
	 
	return retour;  // valeur de retour à revoir , envisager le cas d'erreur !!!!
}
	

// exception.cc 
//      Entry point into the Nachos kernel from user programs.
//      There are two kinds of things that can cause control to
//      transfer back to here from user code:
//
//      syscall -- The user code explicitly requests to call a procedure
//      in the Nachos kernel.  Right now, the only function we support is
//      "Halt".
//
//      exceptions -- The user code does something that the CPU can't handle.
//      For instance, accessing memory that doesn't exist, arithmetic errors,
//      etc.  
//
//      Interrupts (which can also cause control to transfer from user
//      code into the Nachos kernel) are handled elsewhere.
//
// For now, this only handles the Halt() system call.
// Everything else core dumps.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "syscall.h"
#include "userthread.h"
#include "userprocess.h"


#include "addrspace.h"
#include "synchconsole.h"
#include "synch.h"

//----------------------------------------------------------------------
// UpdatePC : Increments the Program Counter register in order to resume
// the user program immediately after the "syscall" instruction.
//----------------------------------------------------------------------
static void
UpdatePC ()
{
    int pc = machine->ReadRegister (PCReg);
    machine->WriteRegister (PrevPCReg, pc);
    pc = machine->ReadRegister (NextPCReg);
    machine->WriteRegister (PCReg, pc);
    pc += 4;
    machine->WriteRegister (NextPCReg, pc);
}



//----------------------------------------------------------------------
// ExceptionHandler
//      Entry point into the Nachos kernel.  Called when a user program
//      is executing, and either does a syscall, or generates an addressing
//      or arithmetic exception.
//
//      For system calls, the following is the calling convention:
//
//      system call code -- r2
//              arg1 -- r4
//              arg2 -- r5
//              arg3 -- r6
//              arg4 -- r7
//
//      The result of the system call, if any, must be put back into r2. 
//
// And don't forget to increment the pc before returning. (Or else you'll
// loop making the same system call forever!
//
//      "which" is the kind of exception.  The list of possible exceptions 
//      are in machine.h.
//----------------------------------------------------------------------

void
ExceptionHandler (ExceptionType which)
{
    int type = machine->ReadRegister (2);

#ifndef CHANGED 

    if ((which == SyscallException) && (type == SC_Halt))
      {
	  DEBUG ('a', "Shutdown, initiated by user program.\n");
	  interrupt->Halt ();
      }
    else
      {
	  printf ("Unexpected user mode exception %d %d\n", which, type);
	  ASSERT (FALSE);
      }

#else // CHANGED

	// *****************************************************************
	// *****************************************************************
	//                      
	//                           APPELS SYSTEME
	//   
	// *****************************************************************
	// *****************************************************************
    if (which == SyscallException) {
      switch (type) {


	// *****************************************************************
	//                      SC_Halt
	//
	//  aucun parametre
	// *****************************************************************
	case SC_Halt: {
		DEBUG ('a', "Shutdown, initiated by user program.\n");
		interrupt->Halt ();
	break;
	}



	// *****************************************************************
	//                      SC_Exit
	//
	//  aucun parametre
	// *****************************************************************
	case SC_Exit: {
		DEBUG ('a', "  \nAppel systeme ******** EXIT ********* \nThread courant : %s \n\n", currentThread->getName() );
		
		ASSERT(currentThread->identifiant == 0); // seul le thread main d'identifiant 0 ....
		
		
		int idSpace = currentThread->space->idAddrSpace;
		 
		machine->semaphoreMachine->P(); // necessaire ?
			machine->RemoveProcess(idSpace);
			int reste = machine->RemainProcess();
			ASSERT(reste>=0);
		machine->semaphoreMachine->V();
		
		//printf("Appel systeme EXIT (%d) il reste : %d et  id : %d\n", idSpace, reste,  currentThread->identifiant );
		
		if (reste>0){
			//interrupt->SetLevel (IntOff);
			
			currentThread->Finish();  
		}  else {
			interrupt->Halt();
		}
	break;
	}
	
	
	
	// *****************************************************************
	//                      SC_PutChar
	//
	//   parametre : car (char)
	// *****************************************************************
	case SC_PutChar: {
		DEBUG ('a', "PutChar, écriture, initiated by user program.\n");
		
		int car = machine->ReadRegister (4);
		synchconsole->SynchPutChar (car);
	break;
	}
	
	
	
	// *****************************************************************
	//                      SC_PutInt
	//
	//  parametre : entier (int)
	// *****************************************************************
	case SC_PutInt: {
		DEBUG ('a', "PutInt, écriture d'un entier.\n");
		
		int entier = machine->ReadRegister (4);
		synchconsole->SynchPutInt (entier);
	break;
	}



	// *****************************************************************
	//                      SC_PutString
	//
	//  parametre : pointeur sur une chaine de caractères
	//      adresse dans le segment de data du processus
	//      (donc n'est pas dans la pile du thread, ce qui evite les problemes
	//          de thread qui n'existe plus avec une pile disparue ....)
	//
	//  attention : longueur de chaine max : MAX_STRING_SIZE
	//  definie dans threads/system.h 
	//            valeur = 30
	// *****************************************************************
	case SC_PutString: {   
		DEBUG ('a', "PutString, écriture, initiated by user program.\n");
		
		int adresse = machine->ReadRegister (4);
		
		//printf("adresse : %d \n", adresse);  // **********************************************************************
		//currentThread->space->PrintTablePage();
		
		char *buffer = new char[MAX_STRING_SIZE+1]; // +1 pour caractere nul
		copyStringFromMachine(adresse, buffer, MAX_STRING_SIZE);
		
		synchconsole->SynchPutString(buffer);
		
		delete buffer;
	break;
	}


	// *****************************************************************
	//                      SC_GetChar
	//
	//  aucun parametre 
	// *****************************************************************
	case SC_GetChar: {
		DEBUG ('a', "GetChar, lecture, initiated by user program.\n");
		
		int car = synchconsole->SynchGetChar ();
		machine->WriteRegister (2, car);
		
	break;
	}
	
	
	// *****************************************************************
	//                      SC_GetString
	//
	//  deux parametres
	//       char * s : adresse dans la pile du thread qui appelle
	//       int n : au maximum n-1 caractères lus; 
	//          la chaine est complétée avec un caractère nul
	//          si un caractere EOF est rencontré : fin de lecture 
	//          si un saut de ligne est rencontré : ajout du saut de ligne et fin de lecture
	//
	//   void
	// *****************************************************************
	case SC_GetString: {  // ******************************************************************************************** EOF à gerer
		DEBUG ('a', "GetString, lecture, initiated by user program.\n");
		
		int adresse = machine->ReadRegister (4); // il faut ecrire à cette adresse
		int nombre = machine->ReadRegister (5); // nombre de caracteres à lire
		
		//printf("GetString, adresse ecriture : %d \n", adresse); // adresse dans la pile du thread !
		if (nombre > MAX_STRING_SIZE)
			nombre = MAX_STRING_SIZE;
			
		if (nombre > 0){
			char *buffer = new char[nombre];
			synchconsole->SynchGetString(buffer, nombre);
						
			// copie dans la mémoire MIPS
			for (int k=0; k<nombre; k++){
				machine->WriteMem(adresse+k, 1, buffer[k]);
			}

			delete buffer;
		}

	break;
	}




	// *****************************************************************
	//                      SC_GetInt
	//
	//  aucun parametre 
	// *****************************************************************
	case SC_GetInt: {
		DEBUG ('a', "GetInt, lecture, initiated by user program.\n");
		int adresse = machine->ReadRegister (4); 
		
		int a=0;
		int *ptr = &a;
		
		synchconsole->SynchGetInt(ptr);
		
		/*
		int resultat = synchconsole->SynchGetInt(ptr);
		if (resultat == 0){
			printf("succes : %d \n", *ptr);
		} else {
			printf("echec : %d \n", *ptr);
		}
		*/
		
		machine->WriteMem(adresse, 4, *ptr);
		
	break;
	}




	// *****************************************************************
	//                      SC_UserThreadCreate
	//
	//     deux parametres
	//           arg1 : fonction    void f(void *arg)
	//           arg2 : argument    void *arg 
	// *****************************************************************
	case SC_UserThreadCreate: {
		 
		DEBUG ('t', "[SC_UserThreadCreate]  ......DEBUG t ...\n");
		
		// ***************************  activer interruptions ??????????????????????????????
		// ou inutile car pas d'interruption en mode noyau ??????????????????
		
		
		int arg1 = machine->ReadRegister(4);  // void f(void *arg)
		int arg2 = machine->ReadRegister(5);  // void * arg
		
		int retour = do_UserThreadCreate(arg1, arg2);
		 
		machine->WriteRegister (2, retour);
		 
		DEBUG ('t', "[SC_UserThreadCreate]  ......(fin) ...\n");
		
	break;
	}

 







	// *****************************************************************
	//                      SC_UserThreadExit
	//
	//     pas de parametre
	// *****************************************************************
	case SC_UserThreadExit: {
		DEBUG ('a', "UserThreadExit, destruction thread, initiated by user program.\n");
		//printf("thread exit rang (%d) identifiant (%d)\n", currentThread->rang, currentThread->identifiant);
		currentThread->space->DeleteThread(currentThread->rang);
		
		currentThread->Finish();
		 
	break;
	}


	// *****************************************************************
	//                      SC_UserThreadJoin
	//
	//     parametre : l'identifiant du thread qui doit se terminer !
	// *****************************************************************
	case SC_UserThreadJoin: {
        DEBUG ('a', "UserThreadJoin attente de la terminaison d'un autre thread.\n");
        
        int identifiant = machine->ReadRegister(4);
  
		//printf(" \nAPPEL SYSTEME JOIN   thread (%s) en attente de : %d ........  \n", currentThread->getName(), identifiant);
		 
		while(currentThread->space->AskForThread(identifiant) >=0){
			currentThread->Yield();
		}
		
		//printf(" \nAPPEL SYSTEME JOIN   thread (%s) en attente de : %d ok \n", currentThread->getName(), identifiant);
	break;
	}


	// *****************************************************************
	//                      SC_GetThreadId
	//
	//     parametre :  
	// *****************************************************************
	case SC_GetThreadId: {
        DEBUG ('a', "SC_GetThreadId \n");
        
        int identifiant = currentThread->identifiant;
		
		machine->WriteRegister (2, identifiant);
	break;
	}
	
	// *****************************************************************
	//                      SC_SemaphoreInit
	//     parametre 1 : nom
	//     parametre 2 : valeur (int)
	
	//   renvoie -1 en cas d'echec
	// renvoie un identifiant entier positif si succes
	// *****************************************************************
	case SC_SemaphoreInit: {
		DEBUG ('a', "Creation d'un semaphore.\n");
		
		//char *name = machine->ReadRegister(4);
		int valeur = machine->ReadRegister(5);
		AddrSpace * space = currentThread->space;
		int idSem = space->AddSemaphore(valeur);
		
		 
		machine->WriteRegister (2, idSem);
		//printf(" \nvaleur du pointeur : %d et %x et %p", (int)ptrSem, (int)ptrSem, ptrSem);
	break;
	}


	// *****************************************************************
	//                      SC_SemP
	//
	//     parametre : identifiant semaphore (int)
	// *****************************************************************
	case SC_SemP: {
		DEBUG ('a', "Semaphore->P \n");
		 
		int idSem =  machine->ReadRegister(4);
		
		AddrSpace * space = currentThread->space;
		space->SemaphoreP(idSem);
		
	break;
	}
	
	
	// *****************************************************************
	//                      SC_SemV
	//
	//     parametre : identifiant semaphore (int)
	// *****************************************************************
	case SC_SemV: {
		DEBUG ('a', "Semaphore->V \n");
		 
		int idSem = machine->ReadRegister(4);
		
		AddrSpace * space = currentThread->space;
		space->SemaphoreV(idSem);
	break;
	}
	
	
	// *****************************************************************
	//                      SC_SemaphoreDestroy
	//
	//     parametre : identifiant semaphore (int)
	// *****************************************************************
	case SC_SemaphoreDestroy: {
		DEBUG ('a', "SemaphoreDestroy->V \n");
		 
		int idSem = machine->ReadRegister(4);
		
		AddrSpace * space = currentThread->space;
		space->RemoveSemaphore(idSem);
	break;
	}



/*
	// *****************************************************************
	//                      SC_SemaphoreInit
	//
	//     parametre : valeur (int)
	// *****************************************************************
	case SC_SemaphoreInit: {
		DEBUG ('a', "Creation d'un semaphore.\n");
		
		//char *name = machine->ReadRegister(4);
		int valeur = machine->ReadRegister(5);
		
		Semaphore *ptrSem = new Semaphore("nouveau", valeur);
		machine->WriteRegister (2, (int)ptrSem);
		//printf(" \nvaleur du pointeur : %d et %x et %p", (int)ptrSem, (int)ptrSem, ptrSem);
	break;
	}


	// *****************************************************************
	//                      SC_SemP
	//
	//     parametre : identifiant semaphore (int)
	// *****************************************************************
	case SC_SemP: {
		DEBUG ('a', "Semaphore->P \n");
		 
		Semaphore *ptrSem = (Semaphore *)machine->ReadRegister(4);
		
		ptrSem->P();
	break;
	}
	
	
	// *****************************************************************
	//                      SC_SemV
	//
	//     parametre : identifiant semaphore (int)
	// *****************************************************************
	case SC_SemV: {
		DEBUG ('a', "Semaphore->V \n");
		 
		Semaphore *ptrSem = (Semaphore *)machine->ReadRegister(4);
		
		ptrSem->V();
	break;
	}
		
*/
	// *****************************************************************
	//                      SC_ForkExec
	//
	//  attention : chemin relatif depuis le répertoire  "nachos/code"
	// *****************************************************************
	case SC_ForkExec: {
		
		DEBUG ('a', "ForkExec \n");
		 
		int adresse = machine->ReadRegister (4); // adresse pour lire chaine de caractère : nom du programme à exécuter
		//printf("*****ForkExec**********\n");
		char *buffer = new char[MAX_STRING_SIZE+1+6]; // +1 pour caractere nul   et   +6 pour ecrire "build/"
		
		copyStringFromMachine(adresse, buffer+6, MAX_STRING_SIZE);
		
		int idProcess = do_UserProcessCreate(buffer);
		delete buffer;
		
		
		machine->WriteRegister (2, idProcess);
	break;
	}
		

	// *****************************************************************
	//                      SC_GetPid
	//
	//  attention : chemin relatif depuis le répertoire  "nachos/code"
	// *****************************************************************
	case SC_GetPid: {
		
		DEBUG ('a', "GetPid \n");
		int pid = currentThread->space->idAddrSpace; 
		machine->WriteRegister (2, pid);
	break;
	}
		

	// *****************************************************************
	//                      SC_AllocEmptyPage
	//
	//  pas de parametre 
	//  cherche une page non utilisée 
	//  et lui associe un cadre en mémoire
	//
	//  attention : renvoie l'adresse virtuelle du 1er octet de la page
	//  (et non l'indice de la page !!!)
	//  la valeur de retour est donc utilisable pour des tests
	//  
	//  renvoie -1 si echec
	// *****************************************************************
	case SC_AllocEmptyPage: {
		
		DEBUG ('a', "AllocEmptyPage \n");
		
		AddrSpace * addrspace = currentThread->space;
		
		// cherche une page libre
		int indice = addrspace->FindEmptyPage();
		
		addrspace->AllocateEmptyPage(indice);
		 
		int pointeur = indice*PageSize;
		machine->WriteRegister (2, pointeur); // on retourne l'adresse virtuelle du 1er octet de la page allouée, 
		                        // doit être modifié (????)  mais permet d'utiliser cette adresse dans les programmes d'application 
	break;
	}



	// *****************************************************************
	//                      SC_FreePage
	//
	//  argument : adresse virtuelle (pointeur) du 1er octet de la 
	//             page à supprimer
	//  donc un multiple de 'PageSize'
	// *****************************************************************
	case SC_FreePage: {
		
		DEBUG ('a', "FreePage \n");
		 
		int adresse = machine->ReadRegister (4); // adresse du 1er octet de la page ...
		ASSERT(adresse%PageSize == 0);
		
		// on calcule l'indice de la page à libérer
		int indicePageVirtuelle = adresse/PageSize;
		
		AddrSpace * addrspace = currentThread->space;
		int codeRetour = addrspace->FreeAddrPage(indicePageVirtuelle);
		
		machine->WriteRegister (2, codeRetour); // on retourne 0, pour reussite
		
	break;
	}
	
	
	
	// *****************************************************************
	//                      SC_Sbrk
	//
	//  argument : nombre N de pages à ajouter (strictement positif !!!)
	//
	//  augmente la taille du tas de N pages
	//  la variable brk est mise à jour
	//   
	//  attention : renvoie l'adresse virtuelle du 1er octet de la page
	//  (et non l'indice de la page !!!)
	//  la valeur de retour est donc utilisable pour des tests
	//  
	//  renvoie -1 si echec
	// *****************************************************************
	case SC_Sbrk: {
		
		DEBUG ('a', "Appel Systeme Sbrk \n");
		
		int nombrePages = machine->ReadRegister (4);
		ASSERT(nombrePages > 0);
		 
		AddrSpace * addrspace = currentThread->space;
		
		int indicePage = addrspace->SbrkAddr(nombrePages);
		
		int adresseVirtuelle;
		if (indicePage>=0){
			adresseVirtuelle = indicePage*PageSize;
		} else {
			adresseVirtuelle = -1;
		}
		// ajouter extension du tas **********************************************
		
		machine->WriteRegister (2, adresseVirtuelle); // on retourne l'adresse virtuelle du 1er octet de la page allouée, 
		                          // doit être modifié (????)  mais permet d'utiliser cette adresse dans les programmes d'application 
	break;
	}



	// *****************************************************************
	//                      SC_HeapPlus
	//
	//  argumente le tas d'une page
	//  renvoie -1 en cas d'echec
	//  renvoie la nouvelle valeur de brk
	// *****************************************************************
	case SC_HeapPlus: {
		
		DEBUG ('a', "HeapPlus\n");
		 
		//int nbBytes = machine->ReadRegister (4); 
		
		AddrSpace * addrspace = currentThread->space;
		 
		
		int indicePage = addrspace->AddPageHeap();
		 
		 
		machine->WriteRegister (2, indicePage); // à convertir !!!!
		
	break;
	}
	
	

	// *****************************************************************
	//                      SC_HeapSize
	//
	//  renvoie le nombre d'octets utilisables dans le tas
	// *****************************************************************
	case SC_HeapSize: {
		
		DEBUG ('a', "HeapSize\n");
		 
		//int nbBytes = machine->ReadRegister (4); 
		
		AddrSpace * addrspace = currentThread->space;
		 
		
		int maxSize = addrspace->HeapSize();
		 
		 
		machine->WriteRegister (2, maxSize); // à convertir !!!!
		
	break;
	}
	


	// *****************************************************************
	//                      SC_Malloc
	//
	//  argument : nombre d'octets STRICTEMENT positif
	//             renvoie un entier
	//  si réussite  : adresse virtuelle du 1er octet alloué
	//  si echec : -1
	// *****************************************************************
	case SC_Malloc: {
		
		DEBUG ('a', "Malloc\n");
		 
		int nbBytes = machine->ReadRegister (4); 
		
		if(nbBytes <= 0){
			machine->WriteRegister (2, -1); // on retourne -1 pour echec
		} else { 
			AddrSpace *space = currentThread->space;
			int adresse = space->MallocHeapSpace(nbBytes);
			machine->WriteRegister (2, adresse); // on retourne adresse
		}
		
		 
	break;
	}
	
	
	



	// *****************************************************************
	//                      SC_Free
	//
	//  argument : adresse (dans mémoire virtuelle) de la zone à libérer
	//   
	// *****************************************************************
	case SC_Free: {
		
		DEBUG ('a', "Malloc\n");
		
		int adresse = machine->ReadRegister (4); // adresse du 1er octet 
		//printf("free : %d\n", adresse);
		AddrSpace *space = currentThread->space;
		
		 
		
		
		space->FreeHeapSpace(adresse); 
		 
	break;
	}
	
	

	// *****************************************************************
	//                     default
	// *****************************************************************
	default: {
		printf ("Unexpected user mode exception %d %d\n", which, type);
		ASSERT (FALSE);
	}


  }  //switch (type)

// *********************************************************************
// *********************************************************************
// *********************************************************************
// *********************************************************************


    // LB: Do not forget to increment the pc before returning!
    UpdatePC ();
    // End of addition

  }  // if (which == SyscallException)
}


#endif // CHANGED

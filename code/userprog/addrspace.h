// addrspace.h 
//      Data structures to keep track of executing user programs 
//      (address spaces).
//
//      For now, we don't keep any information about address spaces.
//      The user level CPU state is saved and restored in the thread
//      executing the user program (see thread.h).
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#ifndef ADDRSPACE_H
#define ADDRSPACE_H

#include "copyright.h"
#include "filesys.h"
 
#include "tablesemaphore.h"
#include "tablethread.h"
#include "heap.h"
#include "bitmap.h"
#include "synch.h"
 

//#define UserStackSize		1024	// increase this as necessary!


#define NumPagesStackMain  3
#define NumThreadsMax  5
#define NumHeapPages   5

#define UserStackNumPages   NumPagesStackMain + NumThreadsMax
#define UserStackSize		(UserStackNumPages*PageSize) 

#define NumSpacePages     (NumPhysPages+10)
#define SpaceMemorySize    (NumSpacePages*PageSize)

class AddrSpace
{
	public:
		AddrSpace (OpenFile * executable);	// Create an address space,
		// initializing it with the program
		// stored in the file "executable"
		~AddrSpace ();		// De-allocate an address space
		
		void InitRegisters ();	// Initialize user-level CPU registers,
		// before jumping to user code
		
		void CopyPageToFrame(int vpn);
		void CopyFrameToPage(int vpn);
		void SaveState ();		// Save/restore address space-specific
		void RestoreState ();	// info on a context switch 
		
		void PrintTablePage();
		
		// gestion des threads
		void DeleteThread(int rang);
		int AjouteThread();
		int EnregistreThread(int rangThread);
		int AskPageStack(int rangThread);
		int AskForThread(int identifiant);
		
		// gestion du tas
		int IncreaseBrk();
		int DecreaseBrk();
		
		int HeapSize();
		int SetUpHeap();
		int FindEmptyPage();
		int AllocateEmptyPage(int indicePage);
		int FreeAddrPage(int indicePage);
		int SbrkAddr(int nombre);
		int MallocHeapSpace(int numBytes);
		void FreeHeapSpace(int adresse);
		int AddPageHeap();
		
		int AddSemaphore(int valeurSem);
		void RemoveSemaphore(int idsem);
		void SemaphoreP(int idsem);
		void SemaphoreV(int idsem);
		
		
		Semaphore * semaphoreGestionThreads;
		
		int idAddrSpace; // identifiant pid
		
		
	
	private:
		TranslationEntry * pageTable;	// Assume linear page table translation
		int numPages;	// nombre de pages utilisées dans l'espace d'adressage
		char *addrSpaceMemory; // ********************************************************
		
		TableThread * nouvelleTableThread;
		TableSemaphore * tableSemaphore;
		
		
		Heap *heap;
		
		int brk; // référence une page
		int restore[NumSpacePages]; // utilisé par la fonction restoreState 
		 
};

#endif // ADDRSPACE_H

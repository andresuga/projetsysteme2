// synchconsole.h 

#ifdef CHANGED 

#ifndef SYNCHCONSOLE_H
#define SYNCHCONSOLE_H

#include "copyright.h"
#include "utility.h"
#include "console.h"




// ************************  extern ???
void copyStringFromMachine(int from, char *to, unsigned size);





// *********************************************************************
// *********************************************************************
// *********             class SynchConsole               **************
// *********************************************************************
// *********************************************************************



class SynchConsole {
	public:
		SynchConsole(char *readFile, char *writeFile);
					// initialize the hardware synchconsole device
		~SynchConsole();			// clean up console emulation
		
	// external interface  
		void SynchPutChar(char ch);
		void SynchPutString(const char *s);
		void SynchPutInt(int nb);
		
		char SynchGetChar();
		void SynchGetString(char *s, int n);
		int SynchGetInt(int *ptr);
	
	
	
	private:
		Console *console;
		};

#endif // SYNCHCONSOLE_H

#endif // CHANGED

// tablethread.h 

#ifdef CHANGED 

#ifndef TABLETHREAD_H
#define TABLETHREAD_H

#include "copyright.h"
#include "utility.h"



class ThreadEntry{
	public :
		int pageStack;
		int identifiant;
		int used;
	};


// ************************************************************************
// ************************************************************************

class TableThread {
	
	public:
		TableThread(int size);
				// initialize the  
		~TableThread();			// clean up console emulation
		
		// external interface 
		
		void Print();
		int Find ();		// Return the # of a clear  element
		int EnregistreElt(int rangThread);
		void DeleteElt(int rangThread);
		int GetPageStack(int rangThread);
		int AskForThread(int identifiant);
		
		
	private:
		int nombreMax;
		int effectif;
		ThreadEntry *tableDesThreads;
		int distributeurIdentifiant;
	};

#endif // TABLETHREAD_H

#endif // CHANGED

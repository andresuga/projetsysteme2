// TableThread
//  gestion des threads d'un addrspace
//   gestion des spaces de la machine

#ifdef CHANGED

#include "copyright.h"
#include "system.h"
#include "tablethread.h"

 

//----------------------------------------------------------------------
//   TableThread::TableThread
//  
//----------------------------------------------------------------------

TableThread::TableThread(int size)
{
	int a = NumSpacePages;
	int b = UserStackNumPages;
	int premierePageStack = a-b;
	
	
	effectif = 0;
	nombreMax = size;
	tableDesThreads = new ThreadEntry[nombreMax];
	distributeurIdentifiant = 1;
	int i;
	for (i = 0; i < nombreMax; i++){   
		tableDesThreads[i].used = -1;
		tableDesThreads[i].pageStack = premierePageStack+i;
		tableDesThreads[i].identifiant = -1;
	}
}



//----------------------------------------------------------------------
// TableThread::~TableThread
// 	Clean up ....
//----------------------------------------------------------------------

TableThread::~TableThread()
{
   delete [] tableDesThreads;
}




//----------------------------------------------------------------------
// Table::Print 
//   	...
//----------------------------------------------------------------------

void
TableThread::Print(){
	int i;
	printf(" \n\n[Table des threads] \n");
	 for (i = 0; i < nombreMax; i++){
		printf("[%2d] : pageStack (%2d)  identifiant (%2d) libre (%2d)\n", i, 
				tableDesThreads[i].pageStack, 
				tableDesThreads[i].identifiant , 
				tableDesThreads[i].used);
		}
}




//----------------------------------------------------------------------
// TableThread::Find
//      Return the number of the first element which is free.
//      
//      If no element are free, return -1.
//----------------------------------------------------------------------

int
TableThread::Find ()
{
	
	int i;
    for (i = 0; i < nombreMax; i++)
	if (tableDesThreads[i].used == -1) // disponible
	  {   
	      tableDesThreads[i].used = 1;
	      effectif ++;
	      return i;
	  }
	//ASSERT(i<0); // si pas d'element disponible, à modifier ****************
    return -1;
}


//----------------------------------------------------------------------
// TableThread::EnregistreElt (int rangThread);
//       
//----------------------------------------------------------------------

int
TableThread::EnregistreElt (int rangThread)
{
	int identifiant = distributeurIdentifiant;
	distributeurIdentifiant++;
	tableDesThreads[rangThread].identifiant = identifiant;
	return identifiant;
}

//----------------------------------------------------------------------
// TableThread::DeleteElt (int rangThread);
//     
//----------------------------------------------------------------------

void
TableThread::DeleteElt (int rangThread)
{
	tableDesThreads[rangThread].used = -1;
	tableDesThreads[rangThread].identifiant = -1;
	effectif++;
	
	 
}

//----------------------------------------------------------------------
// TableThread:: GetPageStack(int rangThread);
//     
//----------------------------------------------------------------------

int
TableThread::GetPageStack(int rangThread)
{
	//printf("getpagestack [%d] %d \n", rangThread, tableDesThreads[rangThread].pageStack);
	return tableDesThreads[rangThread].pageStack;
	 
}

//----------------------------------------------------------------------
// TableThread:: AskForThread(int identifiant);
//     renvoie le rang du thread si présent
//     renvoie -1 sinon
//----------------------------------------------------------------------

int
TableThread::AskForThread(int identifiant)
{
	int i;
    for (i = 0; i < nombreMax; i++)
	if (tableDesThreads[i].identifiant == identifiant){ 
		// trouvé
		//printf("ask for thread identifiant : (%d) ok, RANG %d\n", identifiant, i); 
		return i; 
	} 
	//ASSERT(i<0); // si pas d'element disponible, à modifier ****************
	//printf("ask for thread identifiant : (%d) non trouvé\n", identifiant); 
    return -1;
	 
}


 

#endif


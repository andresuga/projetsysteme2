// frameprovider.h 

#ifdef CHANGED 

#ifndef FRAMEPROVIDER_H
#define FRAMEPROVIDER_H

#include "copyright.h"
#include "utility.h"
  


class FrameProvider {
  public:
    FrameProvider(int size);
				// initialize the  
    ~FrameProvider();			// clean up console emulation

// external interface 
       	 
 void Print();
 
 void Record(int frame, int idSpace); // le frame d'indice 'frame' est utiisé par le space d'identifiant 'idSpace'
 int NumAvailFrame();
 bool Test (int which);	// Is the "nth" frame set?
 int Find (int idSpace);		// Renvoie un identifiant de frame disponible pour ecriture
  int FindIdFrame(int victime);
    int Ask(int idFrame);
    
  private:
    int frameTableSize;
    int *frameTable;
    //int victime; // identifiant de l'addrspace qui libere des frames lorsque nécessaire
};

#endif // FRAMEPROVIDER_H

#endif // CHANGED

// addrspace.cc 
//      Routines to manage address spaces (executing user programs).
//
//      In order to run a user program, you must:
//
//      1. link with the -N -T 0 option 
//      2. run coff2noff to convert the object file to Nachos format
//              (Nachos object code format is essentially just a simpler
//              version of the UNIX executable object code format)
//      3. load the NOFF file into the Nachos file system
//              (if you haven't implemented the file system yet, you
//              don't need to do this last step)
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "addrspace.h"
#include "noff.h"
 
#include "heap.h"
#include "bitmap.h"
 

#include <strings.h>		/* for bzero */

//----------------------------------------------------------------------
// SwapHeader
//      Do little endian to big endian conversion on the bytes in the 
//      object file header, in case the file was generated on a little
//      endian machine, and we're now running on a big endian machine.
//----------------------------------------------------------------------

static void
SwapHeader (NoffHeader * noffH)
{
    noffH->noffMagic = WordToHost (noffH->noffMagic);
    noffH->code.size = WordToHost (noffH->code.size);
    noffH->code.virtualAddr = WordToHost (noffH->code.virtualAddr);
    noffH->code.inFileAddr = WordToHost (noffH->code.inFileAddr);
    noffH->initData.size = WordToHost (noffH->initData.size);
    noffH->initData.virtualAddr = WordToHost (noffH->initData.virtualAddr);
    noffH->initData.inFileAddr = WordToHost (noffH->initData.inFileAddr);
    noffH->uninitData.size = WordToHost (noffH->uninitData.size);
    noffH->uninitData.virtualAddr =
	WordToHost (noffH->uninitData.virtualAddr);
    noffH->uninitData.inFileAddr = WordToHost (noffH->uninitData.inFileAddr);
}







//**********************************************************************
//**********************************************************************
//**********************************************************************
//**********************************************************************
// AddrSpace::AddrSpace
//      Create an address space to run a user program.
//      Load the program from a file "executable", and set everything
//      up so that we can start executing user instructions.
//
//      Assumes that the object code file is in NOFF format.
//
//      First, set up the translation from program memory to physical 
//      memory.  For now, this is really simple (1:1), since we are
//      only uniprogramming, and we have a single unsegmented page table
//
//      "executable" is the file containing the object code to load into memory
//**********************************************************************
//**********************************************************************
//**********************************************************************
//**********************************************************************

AddrSpace::AddrSpace (OpenFile * executable)
{
    NoffHeader noffH;
    int i, size;
	int dataNumPages; 
	// -----------------------------------------------------------------    
	//     addrSpaceMemory : mémoire de l'espace d'adressage 
	// -----------------------------------------------------------------
	
	// on reserve toute la mémoire nécessaire 'sur disque'
	// à modifier par la suite *********************************************************************************
	
	addrSpaceMemory = new char[SpaceMemorySize];
	
	for (i = 0; i < SpaceMemorySize; i++)
		addrSpaceMemory[i] = 0;
	
	// -----------------------------------------------------------------    
	//     pageTable : table des pages de l'espace d'adressage
	// -----------------------------------------------------------------
	 
	pageTable = new TranslationEntry[NumSpacePages]; // allocation mémoire
	
	for (i = 0; i < NumSpacePages; i++){
		pageTable[i].virtualPage = i;	// for now, virtual page # = phys page #
		pageTable[i].physicalPage = -1;
		pageTable[i].valid = FALSE;  // modifié ********************************************
		pageTable[i].readOnly = FALSE;	// if the code segment was entirely on 
		// a separate page, we could set its pages to be read-only
		
		pageTable[i].use = FALSE;
		pageTable[i].dirty = FALSE;
	}
	
	
	// -----------------------------------------------------------------    
	// calcul de la taille nécessaire pour les segments de code et data
	// -----------------------------------------------------------------
	
	executable->ReadAt ((char *) &noffH, sizeof (noffH), 0);
	if ((noffH.noffMagic != NOFFMAGIC) &&
	(WordToHost (noffH.noffMagic) == NOFFMAGIC))
	SwapHeader (&noffH);
	ASSERT (noffH.noffMagic == NOFFMAGIC);
	
	// how big is address space?
	size = noffH.code.size + noffH.initData.size + noffH.uninitData.size;
	// we need to increase the size to leave room for the stack ************************ plus tard
	
	// numPages et size concernent ce qui est copié depuis l'executable
	dataNumPages = divRoundUp (size, PageSize);
	size = dataNumPages * PageSize;
	
	ASSERT (dataNumPages <= NumSpacePages);	//  *****************************************************************  
	
	
	
	// -----------------------------------------------------------------    
	// copy in code segment into addrSpaceMemory
	// -----------------------------------------------------------------
	if (noffH.code.size > 0)
	{
		DEBUG ('a', "Initializing code segment, at 0x%x, size %d\n",
				noffH.code.virtualAddr, noffH.code.size);
		 
		int ecriture1 = executable->ReadAt (
				&(addrSpaceMemory[noffH.code.virtualAddr]),
				noffH.code.size, 
				noffH.code.inFileAddr);
				
		//printf("Premiere ecriture : %d\n", ecriture1);
		ecriture1++; // pour eviter warning à la compilation
    }
	
	// then, copy in data segments into addrSpaceMemory
	if (noffH.initData.size > 0)
	{
		DEBUG ('a', "Initializing data segment, at 0x%x, size %d\n",
				noffH.initData.virtualAddr, noffH.initData.size);
		
		int ecriture2 = executable->ReadAt (
				&(addrSpaceMemory[noffH.initData.virtualAddr]),
				noffH.initData.size, 
				noffH.initData.inFileAddr);
				
		//printf("Seconde ecriture : %d\n", ecriture2);
		ecriture2++;  // pour eviter warning à la compilation
	}
	
	
	// mise à jour de la table des pages pour la partie code + data
	for (i = 0; i < dataNumPages; i++){   
		pageTable[i].use = TRUE;
	}
	
	brk = dataNumPages; // à ce stade, le tas est inexistant
	 
	
	// -----------------------------------------------------------------    
	//         mise en place de la pile/stack
	// -----------------------------------------------------------------
	ASSERT(NumSpacePages - UserStackNumPages > brk + 5); // pour 5 pages de tas
	
	int userStackNumPages = (int)UserStackNumPages;
	int premier = (int)NumSpacePages - userStackNumPages; // à revoir *************************************************************************************
	
	// la pile utilise les pages de plus grand indice de l'espace d'adressage
	for (i = premier; i < NumSpacePages; i++){   
		pageTable[i].use = TRUE;
	}
	 
	numPages = dataNumPages + UserStackNumPages; // code + data + pile
	 
	ASSERT (numPages <= NumSpacePages);	//  pour le moment ... redondant
	 
	 
	// -----------------------------------------------------------------    
	//     tableThreads : table des threads de l'espace d'adressage  
	// -----------------------------------------------------------------
	nouvelleTableThread = new TableThread(NumThreadsMax);
	//nouvelleTableThread->Print();
	
	// -----------------------------------------------------------------    
	//         mise en place du tas / heap
	// -----------------------------------------------------------------
	heap = new Heap(0, 0); // vraiment utile ici ?
	 
	
	// -----------------------------------------------------------------    
	//         semaphore pour gestion des threads
	// -----------------------------------------------------------------
	tableSemaphore = new TableSemaphore(); // semaphores pour users
	
	semaphoreGestionThreads = new Semaphore("gestion des threads", 1);
	 
	// -----------------------------------------------------------------    
	//     identifiant de l'addrspace
	// -----------------------------------------------------------------
	idAddrSpace = machine->AddProcess();
	ASSERT(idAddrSpace >= 1);
	
	
	for (i=0; i<NumSpacePages; i++){
		restore[i]=-1;
	}
	//printf("identifiant : %d \n", idAddrSpace);
	//PrintTablePage();
}



//**********************************************************************
//**********************************************************************
// AddrSpace::~AddrSpace
//      De-alloate an address space.  Nothing for now!
//
//**********************************************************************
//**********************************************************************

AddrSpace::~AddrSpace ()
{
  //printf("******************************* destruction addrspace \n\n"); 
 
 
  // LB: Missing [] for delete
  // delete pageTable;
  delete [] pageTable;
  // End of modification
   
  delete [] addrSpaceMemory;
 
  delete nouvelleTableThread;
  
  delete semaphoreGestionThreads;
  delete heap; 
   
}





//**********************************************************************
//**********************************************************************
// AddrSpace::InitRegisters
//      Set the initial values for the user-level register set.
//
//      We write these directly into the "machine" registers, so
//      that we can immediately jump to user code.  Note that these
//      will be saved/restored into the currentThread->userRegisters
//      when this thread is context switched out.
//**********************************************************************
//**********************************************************************

void
AddrSpace::InitRegisters ()
{
	int i;
	
	for (i = 0; i < NumTotalRegs; i++)
		machine->WriteRegister (i, 0);
	
	// Initial program counter -- must be location of "Start"
	machine->WriteRegister (PCReg, 0);
	
	// Need to also tell MIPS where next instruction is, because
	// of branch delay possibility
	machine->WriteRegister (NextPCReg, 4);
	
	// Set the stack register to the end of the address space, where we
	// allocated the stack; but subtract off a bit, to make sure we don't
	// accidentally reference off the end! 
	
	int initStack = NumSpacePages * PageSize - 16;// *********************************************************************************
	machine->WriteRegister (StackReg, initStack); 
	//printf("pointeur pile : %d \n", initStack);
	
	DEBUG ('a', "Initializing stack register to %d\n",initStack );
	//printf("tas initialisé : %d \n", numPages * PageSize - 16);
}



//**********************************************************************
//**********************************************************************
// AddrSpace::SaveState
//      On a context switch, save any machine state, specific
//      to this address space, that needs saving.
//
//**********************************************************************
//**********************************************************************

void
AddrSpace::SaveState ()
{
	//printf(" \n\n*************** SaveState() - processus [%d]\n", currentThread->space->idAddrSpace);
	int vpn;
	 
	for (vpn = 0; vpn < NumSpacePages; vpn++)
	{
		if (pageTable[vpn].dirty == TRUE && pageTable[vpn].use == TRUE){
			//printf("dirty : page %d \n", vpn);
			 
			pageTable[vpn].dirty = FALSE;
			CopyFrameToPage(vpn);
		}
	}
}


//**********************************************************************
//**********************************************************************
// AddrSpace::RestoreState
//      On a context switch, restore the machine state so that
//      this address space can run.
//
//**********************************************************************
//**********************************************************************

void
AddrSpace::RestoreState ()
{
	machine->pageTable = pageTable;
	machine->pageTableSize = NumSpacePages;  // **********************************************
	
	int indiceFrame, vpn, suivre,  oldIdFrame;
	int nbPagesCopie = 0;
	int compteur = 0;
	
	for (vpn = 0; vpn < NumSpacePages; vpn++){
		suivre = 0;
		// la pge doit-elle être chargée ?
		
		// si la page est utilisée (use == FALSE)
		if (pageTable[vpn].use == TRUE){
			compteur ++;
			// on examine l'ancien cadre de la mémoire physique
			oldIdFrame =  pageTable[vpn].physicalPage;
			
			// si (-1), la page n'a jamais été chargée en mémoire : il faut charger la page
			if (oldIdFrame == -1){ 
				suivre = 1;
				//printf("vpn %d ---  condition 1 --- \n", vpn);
			} 
			// sinon si cet ancien cadre n'est plus attribué à ce processus  : il faut charger la page
			else if (machine->frameProvider->Ask(oldIdFrame) != idAddrSpace){
				suivre = 1;
				//printf("vpn %d ---  condition 2 --- \n", vpn);
			} 
		}  
		// le numero de page est enregistré dans le tableau restore 
		if (suivre){
			restore[nbPagesCopie] = vpn;
			nbPagesCopie ++;
		}
	}
	
	ASSERT(compteur == numPages);
	// toutes les pages enregistrées dans le tableau 'restore' sont chargées en mémoire physique
	for (int i=0; i<nbPagesCopie; i++){
		vpn = restore[i];
		
		indiceFrame = machine->frameProvider->Find(idAddrSpace);
		ASSERT(indiceFrame>=0);
		//printf("RestoreState : page %d chargée dans le cadre %d \n", vpn, indiceFrame);  
		pageTable[vpn].physicalPage = indiceFrame;
		pageTable[vpn].valid = TRUE;
		pageTable[vpn].dirty = FALSE;
		
		CopyPageToFrame(vpn);
		
		machine->frameProvider->Record(indiceFrame, idAddrSpace); 
	}
	 
	/*
	printf(" \n[restoreState() ]--------------------------------------------  [%d] \n", idAddrSpace);
	
	machine->frameProvider->Print();
	PrintTablePage();
	char bu[10];
	fgets(bu, 5, stdin);
	*/ 
}




//----------------------------------------------------------------------
//      AddrSpace::CopyFrameToPage
//      
//          copie le cadre  'pageTable[vpn].physicalPage'
//          vers la page virtuelle 'vpn' 
//----------------------------------------------------------------------
void 
AddrSpace::CopyFrameToPage(int vpn){
	
	machine->execute = 0; // pour passer en mode copie
	
	int nbIteration =  PageSize/4;
	int addrBase = vpn*PageSize; // memoire de la machine
	int nb = 3; // ************ pour initialiser 'valeur' (warning)
	int *valeur = &nb;
	int compt;
	
	for (compt=0; compt<nbIteration; compt++){
		 
		machine->ReadMem(addrBase + compt*4 ,4,  valeur  ); // 1er argument est une adresse virtuelle
		
		*((int *)addrSpaceMemory+ vpn*PageSize/4  + compt) = *valeur; // int donc 4 octets
	}
	
	machine->execute = 1;// pour sortir du mode copie
}



//----------------------------------------------------------------------
//       AddrSpace::CopyPageToFrame
//      
//         copie de la page virtuelle 'vpn' 
//         vers le cadre  'pageTable[vpn].physicalPage'
//----------------------------------------------------------------------
void 
AddrSpace::CopyPageToFrame(int vpn){
	machine->execute = 0; // pour passer en mode copie
	
	int addrBase = vpn*PageSize; // memoire de la machine
	int valeur;
	int compt;
	int nbIteration =  PageSize/4;
	
	for (compt=0; compt<nbIteration; compt++){
		// on lit la valeur dans la mémoire de l'addrspace
		valeur = *((int *)addrSpaceMemory+ vpn*PageSize/4  + compt); // int donc 4 octets
		 
		// pour ecrire dans la mémoire de la machine, utilisation de la fonction translate qui connait l'adresse physique
		// donc idFrame n'est pas utilisé (deja connu grace à la table des pages)
		machine->WriteMem(addrBase + compt*4 ,4,  valeur  ); // 1er argument est une adresse virtuelle
	}
	
	machine->execute = 1;// pour sortir du mode copie
}


//----------------------------------------------------------------------
// AddrSpace::PrintTablePage
//     
//----------------------------------------------------------------------
void
AddrSpace::PrintTablePage()
{
	printf("Table des pages virtuelles de l'espace d'adressage\n");
	for (int i =0; i<NumSpacePages; i++){
		printf("[%2d] - (%2d) - (%2d) - %d - %d - %d - %d \n", i, 
			pageTable[i].virtualPage,  
			pageTable[i].physicalPage,
			pageTable[i].valid,
			pageTable[i].readOnly,
			pageTable[i].use,
			pageTable[i].dirty
			 );	
	}
  
}




// *********************************************************************
// *********************************************************************
// ************          allocation de pages           *****************
// *********************************************************************
// *********************************************************************



//----------------------------------------------------------------------
// AddrSpace::SbrkAddr(int nombrePages)
//     
//         allocation de plusieurs pages virtuelles
//----------------------------------------------------------------------
int
AddrSpace::SbrkAddr(int nombrePages)
{
	// allocation est-elle possible ? 
	if (NumPhysPages - (int)numPages < nombrePages)
		return -1;
	
	// 1ere page allouée : renvoie la valeur actuelle de brk
	//  on conserve ce nombre indice
	int indicePage = IncreaseBrk();  
	
	if (indicePage>0){
		AllocateEmptyPage(indicePage);
	}
		
	// pages suivantes
	if (nombrePages > 1){
		for (int k = 1; k < nombrePages; k++){
			IncreaseBrk();
			AllocateEmptyPage(indicePage+k);
		}
	}
		
	//PrintTablePage(); 
	return indicePage;
}

//----------------------------------------------------------------------
// AddrSpace::AllocateEmptyPage;
//     
//    une page virtuelle (vide) est placée en mémoire physique
//    il n'y a donc pas de transfert de données
//    
//     argument : l'indice vpn de la page à allouer
//     renvoie 0 si succès
//----------------------------------------------------------------------
int
AddrSpace::AllocateEmptyPage(int indicePage)
{
	ASSERT (pageTable[indicePage].use == FALSE); // à transformer en condition avec return -1 si probleme
	
	numPages++; // nombre de pages utilisées
	ASSERT (numPages <= NumPhysPages); //  redondant, sauf si ...
	
	int idFrame = machine->frameProvider->Find(idAddrSpace);  
	ASSERT(idFrame >= 0);              
	
	// mise à jour de frameProvider
	machine->frameProvider->Record(idFrame, idAddrSpace);   
	
	// mise à jour de la table des pages
	pageTable[indicePage].physicalPage = idFrame;   
	pageTable[indicePage].use = TRUE; 
	
	pageTable[indicePage].valid = TRUE;   // utile ?
	pageTable[indicePage].dirty = FALSE;  // utile ?
	pageTable[indicePage].readOnly = FALSE;  // utile ?
	
	//printf("allocate ... %d \n", indicePage);
	//PrintTablePage();
	return 0; // code succès  ************************************************************  à modifier : -1 si ....
}

 
//----------------------------------------------------------------------
// AddrSpace::FindEmptyPage
//     
//----------------------------------------------------------------------
int
AddrSpace::FindEmptyPage()
{ 
	// l'espace d'adressage virtuel est-il plein ?
	
	if (numPages >= NumSpacePages){ // pour l'instant on ne dépasse pas le nombre de frames  ************************************* modifié
		printf("**************************** \nmémoire pleine ! \n**************************** \n");
		return -1;
	}
	
	// il reste de la place 
	// recherche d'une page virtuelle disponible
	int indice = 0;
	int dispo = pageTable[indice].use;
	while (dispo !=0 && indice<NumPhysPages){
		indice ++;
		dispo = currentThread->space->pageTable[indice].use;
	}
	ASSERT (numPages < NumPhysPages); // *********************************************************************** attention
	
	return indice;
}
 
//----------------------------------------------------------------------
// AddrSpace::FreeAddrPage
//     
//----------------------------------------------------------------------
int
AddrSpace::FreeAddrPage(int indicePage)
{
	int idFrame = pageTable[indicePage].physicalPage;
	//printf("indice cadre: %d \n", idFrame);
	
	// mise à jour du frameprovider
	machine->frameProvider->Record(idFrame, -1); 
	
	// mise à jour de la table des pages
	pageTable[indicePage].use = FALSE;
	numPages--; // nombre de pages utilisées
	
	//PrintTablePage();
	int codeRetour = 0; // -1 si problème ???????????????????????????????????????????
	return codeRetour;
}



//----------------------------------------------------------------------
// AddrSpace::IncreaseBrk
//     
//----------------------------------------------------------------------
int
AddrSpace::IncreaseBrk()
{
	if (pageTable[brk].use == TRUE){
		printf("Plus de place dans l'espace d'adressage !!!\n *** AddrSpace::IncreaseBrk()\n");
		return -1;
	}
	
	int oldBrk = brk;
	 
	
	brk ++;
	return oldBrk;
}



//----------------------------------------------------------------------
// AddrSpace::DecreaseBrk
//     
//----------------------------------------------------------------------
int
AddrSpace::DecreaseBrk()
{
	int oldBrk = brk;
	//printf("DecreaseBrk, valeur initiale : %d \n", oldBrk);
	//PrintTablePage();
	
	brk --;
	return oldBrk;
}





// *********************************************************************
// *********************************************************************
// ***********             gestion du tas                    ***********
// *********************************************************************
// *********************************************************************

//----------------------------------------------------------------------
// AddrSpace::SetUpHeap()
//         
//            retourne
//                adresse du tas si succes
//                -1  si echec
//----------------------------------------------------------------------
int
AddrSpace::SetUpHeap()
{
	int nombrePagesTas = NumHeapPages;  // 5
	
	// allocation des pages
	int indicePage = SbrkAddr(nombrePagesTas);
	
	int adresseTas = -10;
	adresseTas++; // pour eviter warning
	if (indicePage>=0){
		adresseTas = indicePage*PageSize;
		  
		
	} else {
		adresseTas = -1; // echec
		printf("probleme avec le tas ********************************************* \n");
		PrintTablePage();
		ASSERT(FALSE);
	}
	
	heap->SetMemoryAdr(indicePage);
	heap->SetMemorySize(nombrePagesTas);
	heap->HeapInit();
		/*
		PrintTablePage();
		char bu[10];
		fgets(bu, 5, stdin);
		*/
	//printf("creation du tas \n");
	
	//machine->frameProvider->Print();
	return adresseTas;
}


//----------------------------------------------------------------------
// AddrSpace::MallocHeapSpace(int numBytes)
//        
//               réalise la translation d'adresse
//----------------------------------------------------------------------
int
AddrSpace::MallocHeapSpace(int numBytes)
{ 
	int adresse = heap->HeapAlloc(numBytes); // adresse locale du heap (avec origine = 0)
	if (adresse == -1){
		printf("Echec de l'allocation \n");
	} else {
		adresse = adresse + heap->GetMemoryAdr(); // adresse dans la mémoire virtuelle
		//printf("adresse malloc : %d \n", adresse);
	}
	return adresse;
}

//----------------------------------------------------------------------
// AddrSpace::FreeHeapSpace(int numBytes)
//    
//               réalise la translation d'adresse 
//----------------------------------------------------------------------
void 
AddrSpace::FreeHeapSpace(int adresse)
{ 
	adresse = adresse - heap->GetMemoryAdr(); // adresse relative dans le heap
	
	ASSERT(adresse%4 == 0);
	ASSERT(adresse>0);
	int borne = heap->GetMemorySize();
	ASSERT(adresse<borne);
	
	heap->HeapFree(adresse);
}

//----------------------------------------------------------------------
// AddrSpace::AddPageHeap();
//    
//               pour connaitre le nombre d'octets max du tas lorsque celui-ci est vide
//----------------------------------------------------------------------
int
AddrSpace::AddPageHeap()
{ 
	int indicePage = SbrkAddr(1);
	if (indicePage>0){
		heap->AddPageMemory();
	}
	return indicePage;
}

//----------------------------------------------------------------------
// AddrSpace::HeapSize()
//    
//               pour connaitre le nombre d'octets max du tas lorsque celui-ci est vide
//----------------------------------------------------------------------
int
AddrSpace::HeapSize()
{ 
	return (heap->GetMemorySize() - 8);
}

 
// *********************************************************************
// *********************************************************************
// ***********   gestion des threads au sein du processus    ***********
// *********************************************************************
// *********************************************************************

//----------------------------------------------------------------------
// AddrSpace::AjouteThread() 
//----------------------------------------------------------------------
int
AddrSpace::AjouteThread()
{
	int rang = nouvelleTableThread->Find();
	//printf("AjouteThread : rang(%d) \n", rang);
	//nouvelleTableThread->Print();
	return rang;
}


//----------------------------------------------------------------------
// AddrSpace::EnregistreThread(int rangThread);
//----------------------------------------------------------------------
int
AddrSpace::EnregistreThread(int rangThread)
{
	 
	int identifiant = nouvelleTableThread->EnregistreElt(rangThread);
	//printf("EnregistreThread : identifiant (%d) \n", identifiant);
	//nouvelleTableThread->Print();
	return identifiant;
}


//----------------------------------------------------------------------
// AddrSpace::DeleteThread(int identifiant)
//     
//----------------------------------------------------------------------
void
AddrSpace::DeleteThread(int rang)
{
	nouvelleTableThread->DeleteElt(rang);
}


//----------------------------------------------------------------------
// AddrSpace::AskPageStack(int rangThread);
//----------------------------------------------------------------------
int
AddrSpace::AskPageStack(int rangThread)
{
	return nouvelleTableThread->GetPageStack(rangThread);
}


//----------------------------------------------------------------------
// AddrSpace::AskForThread(int identifiant);
//----------------------------------------------------------------------
int
AddrSpace::AskForThread(int identifiant)
{
	return nouvelleTableThread->AskForThread(identifiant);
}


 
// *********************************************************************
// *********************************************************************
// ***********            gestion des semaphores             ***********
// *********************************************************************
// *********************************************************************
 
		
//----------------------------------------------------------------------
// AddrSpace::AddSemaphore(int valeurSem)
//----------------------------------------------------------------------
int
AddrSpace::AddSemaphore(int nbr)
{
	if (tableSemaphore->Full())
		return -1;
	Semaphore *ptrSem = new Semaphore("nouveau", nbr);
	return tableSemaphore->Add((int)ptrSem);
}
		
//----------------------------------------------------------------------
// AddrSpace::RemoveSemaphore(int idSem)
//----------------------------------------------------------------------
void
AddrSpace::RemoveSemaphore(int idSem)
{
	tableSemaphore->Remove(idSem);
}
		
//----------------------------------------------------------------------
// AddrSpace::SemaphoreP(int idSem)
//----------------------------------------------------------------------
void
AddrSpace::SemaphoreP(int idSem)
{
	Semaphore *ptrSem = (Semaphore *)tableSemaphore->Ask(idSem);
		
	ptrSem->P();
}
 
		
//----------------------------------------------------------------------
// AddrSpace::SemaphoreV(int idSem)
//----------------------------------------------------------------------
void
AddrSpace::SemaphoreV(int idSem)
{
	Semaphore *ptrSem = (Semaphore *)tableSemaphore->Ask(idSem);
		
	ptrSem->V();
}
 

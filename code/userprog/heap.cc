// synchconsole.cc 
//  ajout de la synchronisation

#ifdef CHANGED

#include "heap.h"
#include "system.h"
#include "synch.h"



//----------------------------------------------------------------------
// Heap::Heap
//    pointeurPage est la valeur renvoyée par la méthode AddrSpace::SbrkAddr(int nombrePages)
//----------------------------------------------------------------------

Heap::Heap(int nbPages, int pointeurPage)
{
	heapSize = nbPages*PageSize;
	
	addressFirst = pointeurPage*PageSize; // adresse du 1er octet du tas
	 
	addressePointeurAllocationFB = -2;  
	adresseAllocationFB = -2;
	sizeUB = 4;
	minSizeBlock = 8; // 12 ???
}



//----------------------------------------------------------------------
// Heap::~Heap
//  
//----------------------------------------------------------------------

Heap::~Heap()
{
     
}


//----------------------------------------------------------------------
//          Heap::HeapInit()
//  
//----------------------------------------------------------------------
void 
Heap::HeapInit(){
	int memFirst = 0;
	
	int sizeInt = sizeof(int);
	ASSERT(sizeInt == 4 );
	
	// calcul de l'adresse de la premiere structure fb 
	int adressePremierFB = memFirst + sizeInt; // donc 4 !
	// ecriture de cette adresse  
	WriteHeap(memFirst, 4, adressePremierFB);
	
	// ecriture du premier FB
	EcrireDataFB(adressePremierFB,  -1, heapSize - sizeInt);
}



//----------------------------------------------------------------------
//          Heap::HeapAlloc()
//  
//----------------------------------------------------------------------
int
Heap::HeapAlloc(int nbBytes){
	
	//int nbDemande = calculSize(size); // calcul du nombre d'octets nécessaires 
	int nbDemande;
	if (nbBytes %4 ==0){
		nbDemande = sizeUB + nbBytes;
	} else {
		nbDemande = sizeUB + (1 + nbBytes/4)*4;
	}
	//if (nbBesoin < sizeof(struct fb) +sizeof(size_t)){nbBesoin = sizeof(struct fb) +sizeof(size_t) ;} // pour anticiper la liberation de la zone memoire
	 
	// appel à la méthode first fit : on recupere un pointeur sur fb
	int codeRetour = HeapFitFirst(nbDemande );
	
	
	// echec de l'allocation  ------------------------------------------
	if (codeRetour == -1){ 
		//printf("echec allocation   \n");
		return -1;
		}
	
	
	// sinon allocation ok (codeRetour == 0)  -------------------------- 
	int nbDispo = LireSizeFB(adresseAllocationFB);
	int nbReste = nbDispo - nbDemande;
	
	if (nbReste < minSizeBlock){
		nbDemande = nbDispo;
		nbReste = 0;
		}
	
	int addressNextFB = LireNextFB(adresseAllocationFB); 
	
	EcrireUB(adresseAllocationFB, nbDemande);
	
	// premier cas
	if (nbReste>0){
		ASSERT(nbReste>=minSizeBlock);
		EcrireDataFB(adresseAllocationFB+nbDemande, addressNextFB, nbReste);
		
		EcrireNextFB(addressePointeurAllocationFB, adresseAllocationFB+nbDemande);
		}
	// second cas
	else {
		 
		//EcrireNextFB(addressePointeurAllocationFB, adresseAllocationFB+nbDemande);
		EcrireNextFB(addressePointeurAllocationFB, addressNextFB);
		}
	
	return adresseAllocationFB + sizeUB; // adresse relative dans le tas
}



//----------------------------------------------------------------------
// Heap::HeapFree
//  
//----------------------------------------------------------------------
void
Heap::HeapFree(int adresseUB)
{
    int tailleUB;
    ReadHeap(adresseUB-4, 4, &tailleUB);
    
    int addressPreviousFB = SearchPreviousFB(adresseUB-4);
    int addressNextFB = LireNextFB(addressPreviousFB);
    //printf("heapfree : adresse (%d) taille(%d) adresse previousFB (%d)  adresse nextFB(%d) \n", adresseUB, tailleUB, addressPreviousFB, addressNextFB);
    
    EcrireDataFB(adresseUB-4, addressNextFB, tailleUB);
    EcrireNextFB(addressPreviousFB, adresseUB-4);
    
    // 1ere fusion : à droite
    if (FusionDroite(adresseUB-4)){
		//printf("fusion à droite ok \n");
		EcrireDataFB(adresseUB-4, 
					LireNextFB(addressNextFB), 
					LireSizeFB(addressNextFB) + tailleUB);
		EcrireNextFB(addressPreviousFB, adresseUB-4);
	}
	 
    // 2nde fusion : à gauche
    if (addressPreviousFB>0 && FusionDroite(addressPreviousFB)){ // pas de fusion à gauche si le FB precedent à pour adresse 0
		//printf("fusion à gauche ok \n");
		EcrireDataFB(addressPreviousFB, 
					LireNextFB(adresseUB-4), 
					LireSizeFB(adresseUB-4) + LireSizeFB(addressPreviousFB)  );
	} 
}



//----------------------------------------------------------------------
// Heap::SetMemoryAdr
//  
//----------------------------------------------------------------------
void
Heap::SetMemoryAdr(int indicePage)
{
    
    addressFirst = indicePage*PageSize;
}

//----------------------------------------------------------------------
// Heap::GetMemoryAdr
//  
//----------------------------------------------------------------------
int
Heap::GetMemoryAdr()
{
    return addressFirst;
}

//----------------------------------------------------------------------
// Heap::SetMemorySize
//  
//----------------------------------------------------------------------
void
Heap::SetMemorySize(int nbPages)
{
    heapSize = nbPages*PageSize;
}

//----------------------------------------------------------------------
// Heap::GetMemorySize
//  
//----------------------------------------------------------------------
int
Heap::GetMemorySize()
{
    return heapSize;
}

//----------------------------------------------------------------------
// Heap::void AddPageMemory();
//      modifie le champ heapSize
//      met à jour la liste chainee des FB
//----------------------------------------------------------------------
void
Heap::AddPageMemory()
{
    int a = AddressLastFB();
    int size1 = GetMemorySize();
    
    if (a==0){//plus de FB !
		//printf("++++++++++++++ cas1 \n");
		EcrireDataFB(size1, -1, (int)PageSize);
		EcrireNextFB(0, size1);
	} else {
			
		//printf("++++++++++++++ cas2 \n"); 
		int b = LireSizeFB(a);
		if (a+b == size1){
			// le dernier octet est un FB
			EcrireDataFB(a, -1, b+(int)PageSize);
			
		} else {
		// le dernier octet est un UB
			//printf("++++++++++++++ cas3 \n");
			EcrireDataFB(size1, -1, (int)PageSize);
			EcrireDataFB(a, size1, b );
		}
	}
	ASSERT(size1 == heapSize);
	heapSize = heapSize + (int)PageSize;
}


// *********************************************************************
// *********************************************************************
// *********                 private                     ***************
// *********************************************************************
// *********************************************************************


//----------------------------------------------------------------------
// Heap::WriteHeap
//      Write "nbBytes" (1, 2, or 4) bytes of the contents of "value" into
//	virtual memory at location "addr".
//
//   	Returns FALSE if the translation step from virtual to physical memory
//   	failed.
//
//	"heapAdress" -- the virtual address to write to (origine = 0)
//	"nbBytes" -- the number of bytes to be written (1, 2, or 4)
//	"value" -- the data to be written
//----------------------------------------------------------------------
bool
Heap::WriteHeap(int heapAdress, int nbBytes, int value)
{
	bool b = machine->WriteMem(heapAdress + addressFirst, nbBytes, value);
	return b;
}



//----------------------------------------------------------------------
// Heap::ReadHeap
//      Read "size" (1, 2, or 4) bytes of virtual memory at "addr" into 
//	the location pointed to by "value".
//
//   	Returns FALSE if the translation step from virtual to physical memory
//   	failed.
//
//	"heapAdress" -- the virtual address to read from (origine = 0)
//	"nbBytes" -- the number of bytes to read (1, 2, or 4)
//	"value" -- the place to write the result
//----------------------------------------------------------------------
bool
Heap::ReadHeap(int heapAdress, int nbBytes, int *value)
{
	bool b = machine->ReadMem(heapAdress + addressFirst, nbBytes, value);
	return b;
}

  

//----------------------------------------------------------------------
//          Heap::EcrireNextFB(int addressFB, int addressNextFB)
//                 // 1er argument : adresse ou il faut ecrire
//               2nd argument : donnee à ecrire
//----------------------------------------------------------------------
void 
Heap::EcrireNextFB(int addressFB, int addressNextFB){
	ASSERT(addressFB%4 == 0); // verifier l'alignement
	ASSERT(addressNextFB%4 == 0 or addressNextFB == -1); // verifier l'alignement
	
	WriteHeap(addressFB, 4, addressNextFB);
}



//----------------------------------------------------------------------
//          Heap::LireSizeFB(int addressFB)
//  
//----------------------------------------------------------------------
int
Heap::LireSizeFB(int addressFB){
	ASSERT(addressFB%4 == 0); // verifier l'alignement
	int buffer;
	
	ReadHeap(addressFB+4, 4, &buffer);
	return buffer;
}



//----------------------------------------------------------------------
//          Heap::AddressLastFB()
//  renvoie l'adresse de la dernière structure FB
//----------------------------------------------------------------------
int
Heap::AddressLastFB(){
	int a = AddressFirstFB();
	if (a == -1){
		// pas de FB, heap plein
		return 0;
	}
	int suivant = LireNextFB(a);
	while(suivant != -1){
		a = suivant;
		suivant = LireNextFB(a);
	}
	return a;
}

//----------------------------------------------------------------------
//          Heap::AddressFirstFB()
//  
//----------------------------------------------------------------------
int 
Heap::AddressFirstFB(){
	int mem = 0;
	int adresseLue;
	ReadHeap(mem, 4, &adresseLue);
	return adresseLue;
}

//----------------------------------------------------------------------
//          Heap::LireNextFB(int addressFB)
//  
//----------------------------------------------------------------------
int
Heap::LireNextFB(int addressFB){
	ASSERT(addressFB%4 == 0); // verifier l'alignement
	int buffer;
	
	ReadHeap(addressFB, 4, &buffer);
	return buffer;
}



//----------------------------------------------------------------------
//          Heap::EcrireUB(int address,  int nbBytes)
//  nbBytes contient les metadonnees
//----------------------------------------------------------------------
void 
Heap::EcrireUB(int address,  int nbBytes){
	ASSERT(address%4 == 0); // verifier l'alignement
	
	WriteHeap(address, 4, nbBytes);
	
}



//----------------------------------------------------------------------
//          Heap::EcrireDataFB(int address, int AdressNextFB, int sizeFB)
//  
//----------------------------------------------------------------------
void 
Heap::EcrireDataFB(int address, int AdressNextFB, int sizeFB){
	ASSERT(address%4 == 0); // verifier l'alignement
	
	WriteHeap(address, 4, AdressNextFB);
	WriteHeap(address+4, 4, sizeFB);
}
 





//----------------------------------------------------------------------
//          Heap::HeapFitFirst(int nbBytes);
//         renvoie l'adresse (int) d'un FB
//          -1 si echec
//----------------------------------------------------------------------
int
Heap::HeapFitFirst(int nbBytes){
	
	
	int addressFirstFB = AddressFirstFB();
	if (addressFirstFB == -1) {return -1; }
	 
	int old = 0;  
	
	int pointeurCurrentFB = addressFirstFB;
	
	int sizeCurrentFB = LireSizeFB(addressFirstFB); 
	int pointeurNextFB =  LireNextFB(addressFirstFB); 
	 
	
	while(sizeCurrentFB  < nbBytes && pointeurNextFB != -1){
		old = pointeurCurrentFB;
		pointeurCurrentFB = pointeurNextFB;
		
		sizeCurrentFB = LireSizeFB(pointeurCurrentFB); 
		pointeurNextFB = LireNextFB(pointeurCurrentFB); 
	}
	
	if (sizeCurrentFB  < nbBytes){
		return -1;
	}  
	
	addressePointeurAllocationFB = old;
	
	adresseAllocationFB = pointeurCurrentFB;
	
	return 0;
}
 


//----------------------------------------------------------------------
// Heap::FusionDroite(int addressFB)
//  
//----------------------------------------------------------------------
bool
Heap::FusionDroite(int addressFB)
{
	int taille = LireSizeFB(addressFB);
	int suivant = LireNextFB(addressFB);
	 
	if (suivant == -1){
		return FALSE;
	}
	
	if (addressFB + taille == suivant){
		return TRUE;
	} else {
		return FALSE;
	}
}



//----------------------------------------------------------------------
// Heap::SearchPreviousFB(int address)
//  
//----------------------------------------------------------------------
int
Heap::SearchPreviousFB(int addressRef) 
{
	int adresse1 = 0;
	int adresse2 = LireNextFB(adresse1);
	int temp;
	while(adresse2 != -1 && adresse2 < addressRef){
		temp = adresse2;
		adresse2 = LireNextFB(adresse2);
		adresse1 = temp;
		}
	return adresse1;
	 
}



#endif


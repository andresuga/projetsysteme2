// TableSemaphore
//  gestion des semaphores d'un addrspace
//   
#ifdef CHANGED

#include "copyright.h"
#include "system.h"
#include "tablesemaphore.h"

 

//----------------------------------------------------------------------
//   TableSemaphore::TableSemaphore
//  
//----------------------------------------------------------------------

TableSemaphore::TableSemaphore( )
{
	sizeTable = NumSemaphores; // constante 
	effectif = 0;
	
	tableDesSemaphores = new SemaphoreEntry[sizeTable];
	distributeurIdentifiant = 0;
	
	int i;
	for (i = 0; i < sizeTable; i++){   
		tableDesSemaphores[i].valeurSem = -1;
		tableDesSemaphores[i].idSem = -1;
		 
	}
}



//----------------------------------------------------------------------
// TableSemaphore::~TableSemaphore
// 	Clean up ....
//----------------------------------------------------------------------

TableSemaphore::~TableSemaphore()
{
   delete [] tableDesSemaphores;
}


 


//----------------------------------------------------------------------
// TableSemaphore::Add(int valeurSem)
//      si 
//      
//      If no element are free, return -1.
//----------------------------------------------------------------------

int
TableSemaphore::Add(int valeurSem)
{
	ASSERT(effectif < sizeTable);
	
	int rang = SearchFreePlace();
	effectif++;
	distributeurIdentifiant++;
	
	tableDesSemaphores[rang].idSem = distributeurIdentifiant;
	tableDesSemaphores[rang].valeurSem = valeurSem;
	 
    return distributeurIdentifiant;
}


//----------------------------------------------------------------------
// TableSemaphore::Remove(int idSem)
//      
//----------------------------------------------------------------------

void
TableSemaphore::Remove(int idSem)
{
	int rang = SearchRang(idSem);
	effectif--;
	 
	tableDesSemaphores[rang].idSem = -1;
}

//----------------------------------------------------------------------
// TableSemaphore::Ask(int idSem)
//      
//----------------------------------------------------------------------

int
TableSemaphore::Ask(int idSem)
{
	int rang = SearchRang(idSem);
	 
	return tableDesSemaphores[rang].valeurSem;
}

 

//----------------------------------------------------------------------
// TableSemaphore::NotFull()
//      Return 1 s'il reste de la place
//      
//     return 0 si plein.
//----------------------------------------------------------------------

int
TableSemaphore::Full()
{
	if (effectif < sizeTable)
		{return 0;}
    return 1;
}
 

//----------------------------------------------------------------------
// TableSemaphore::SearchRang(int idSem)
//      
//----------------------------------------------------------------------

int
TableSemaphore::SearchRang(int idSem)
{
	int rang = 0;
	int id;
	while(rang<sizeTable){
		id = tableDesSemaphores[rang].idSem;
		if (id == idSem)
			{return rang;}
		rang++;
	}
	 
    return -1;
}

//----------------------------------------------------------------------
// TableSemaphore::SearchFreePlace()
//      
//----------------------------------------------------------------------

int
TableSemaphore::SearchFreePlace()
{
	 
    return SearchRang(-1);
}

 

 

#endif


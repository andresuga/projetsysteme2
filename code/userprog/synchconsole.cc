// synchconsole.cc 
//  ajout de la synchronisation

#ifdef CHANGED

#include "copyright.h"
#include "system.h"
#include "synchconsole.h"
#include "synch.h"





// *********************************************   extern ????????????????? 
 
void copyStringFromMachine(int from, char *to, unsigned size){
	 
	int res;  // car la methode ReadMem utilise un int
	unsigned int k=0;
	machine->ReadMem(from, 1, &res);
    
    while(res !=0 && k<size){
		to[k] = (char)res;
		 
		k++;
		machine->ReadMem(from+k, 1, &res);
	}
	
	to[k] = 0; // fin de chaine
}
 





// *********************************************************************
// *********************************************************************
// *********             class SynchConsole               **************
// *********************************************************************
// *********************************************************************

static Semaphore *readAvail;
static Semaphore *writeDone;

static Semaphore *protegeEcriture;
static Semaphore *protegeLecture;

static void ReadAvail(int arg){ readAvail->V();  }
static void WriteDone(int arg){ writeDone->V();  }
 


//----------------------------------------------------------------------
// SynchConsole::SynchConsole
//  
//----------------------------------------------------------------------
SynchConsole::SynchConsole(char *readFile, char *writeFile)
{
    readAvail = new Semaphore("read avail", 0);
    writeDone = new Semaphore("write done", 0);
     
    protegeEcriture = new Semaphore("protege ecriture", 1);
    protegeLecture = new Semaphore("protege lecture", 1);
    
    console = new Console(&*readFile, &*writeFile, ReadAvail, WriteDone, 0);
}



//----------------------------------------------------------------------
// SynchConsole::~SynchConsole
//  
//----------------------------------------------------------------------
SynchConsole::~SynchConsole()
{
    delete console;
    delete writeDone;
    delete readAvail;
    delete protegeEcriture;
}



//----------------------------------------------------------------------
// SynchConsole::SynchPutChar()
// 	
//
//----------------------------------------------------------------------
void
SynchConsole::SynchPutChar(const char ch)
{
    protegeEcriture->P(); // ******** nécessaire !
    console->PutChar(ch);
    writeDone->P();
    protegeEcriture->V();
}



//----------------------------------------------------------------------
// SynchConsole::SynchPutString(const char *s)
// 	
//
//----------------------------------------------------------------------
void
SynchConsole::SynchPutString(const char *s)
{
    int compt = 0;
    char ch=*(s+compt);
    
    protegeEcriture->P();   
    while (ch != 0){
        console->PutChar(ch); // on utilise le champ 'console' !!!!!!!!!!!!!! pas d'appel à SynchPutChar() !!
		writeDone->P();
        compt++;
        ch = *(s+compt);          
    }
	protegeEcriture->V();
}



//----------------------------------------------------------------------
// SynchConsole::SynchPutInt()
// 	
// convertit un entier positif en une chaine de caractère pui s affichage
//
//   utilisable pour les entiers 
//      avec 8 chaiffres max pour les négatifs
//      et 9 chiffres max pour les positifs : max =  10^9 - 1 (9 chiffres)
//
//----------------------------------------------------------------------
void
SynchConsole::SynchPutInt(int nb)
{
	// à completer  *************************************************************************
	ASSERT(nb<1000000000);// 9 chiffres + caractere fin chaine = 10
	ASSERT(nb>-100000000); // 8 chiffres + signe + caractere fin chaine = 10
	// à completer  *************************************************************************
	
	
	int dim = 10;
	char car[dim];
	car[dim-1]=0; // marqueur de fin de chaine
	int pos = dim-2;
	
	int r=0;
	int k;
	
	// code ok mais à reprendre
	if (nb>=0){
	    for(k=0; k<9; k++){
			r = nb%10;
			car[pos] = (char)r + 48;
			nb = (nb-r)/10;
			pos --;
	    }
	    
	    k=0;  // on enlève les '0' à gauche 
	    while(car[k]=='0' && k<8)
	         k++;
	}

	if (nb<0){
		nb = -nb;
	    for(k=0; k<9; k++){
			r = nb%10;
			car[pos] = (char)r + 48;
			nb = (nb-r)/10;
			pos --;
		}
		
		k=0;  // on enlève les '0' à gauche 
		while(car[k]=='0' && k<8)
			 k++;
		
		k--;
		car[k]='-';
	}
	
	// ecriture de la chaine
	SynchPutString(&car[k]);
}



//----------------------------------------------------------------------
// SynchConsole::SynchGetChar()
// 	
//
//----------------------------------------------------------------------
char
SynchConsole::SynchGetChar()
{
    char ch;
    readAvail->P();
    ch = console->GetChar();
    return ch;
}


//----------------------------------------------------------------------
// SynchConsole::SynchGetInt()
// 	
//
//----------------------------------------------------------------------
int
SynchConsole::SynchGetInt(int *ptr)
{
		int N = 10;
		char *buffer = new char[N]; // au maximum N-1 chiffres, signe compris
		SynchGetString(buffer, N);
		
		int compteur=0;
		char car=buffer[0];
		while (car != 0 && car != 10 && compteur<10){
				compteur++;
				car = buffer[compteur];
			}
		 
		
		int nombre = 0;
		// nombre positif
		if (buffer[0]>47 && buffer[0]<58){
			for (int i = 0; i<compteur; i++){
				if (!(buffer[i]>47 && buffer[i]<58)) {return -1;}
				nombre = nombre *10 + (int)(buffer[i]-48);
			}
		}
		// nombre négatif
		else if (buffer[0] == 45){
			for (int i = 1; i<compteur; i++){
				if (!(buffer[i]>47 && buffer[i]<58)) {return -1;}
				nombre = nombre *10 + (int)(buffer[i]-48);
			}
			nombre = nombre*(-1);
		}
		// erreur de saisie
		else {
			return -1;
		}
		
		// copie pour transmission
		*ptr = nombre;
		
		delete buffer;
		return 0;
}



//----------------------------------------------------------------------
// SynchConsole::SynchGetString()
// 	
//
//----------------------------------------------------------------------
void 
SynchConsole::SynchGetString(char *s, int nombre)
{
	int car;
	int k;
	
	protegeLecture->P();
	for(k =0; k<nombre-1; k++){
		car = synchconsole->SynchGetChar ();
		*(s+k) = car; 
		if ((car == 0) || (car == 10) )// 10 pour retour chariot si saisie console  
		break;
	}
	protegeLecture->V();
	if (k==nombre-1){
		*(s+k) = 0;
	} else {
		*(s+k+1) = 0;
	}
	
	 
	
	/*
	for(k =0; k<nombre-1; k++){
		car = synchconsole->SynchGetChar ();
		machine->WriteMem(adresse + k, 1, car);
		if ((car == 0) || (car == 10) )// 10 pour retour chariot si saisie console  
		break;
	}
	
	if (k==nombre-1){
		machine->WriteMem(adresse + k, 1, 0);
		retour = -1;
	} else {
		machine->WriteMem(adresse + k+1, 1, 0);
		retour = 0;
	}
	*/
	 
}




#endif

